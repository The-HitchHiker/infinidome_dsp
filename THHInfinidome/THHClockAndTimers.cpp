/*
 * THHClockAndTimers.cpp
 *
 *  Created on: Jul 13, 2021
 *      Author: AmichaiYifrach
 */

#include "THHClockAndTimers.h"
//-------------------------------------------------------------

Types_FreqHz        SystemClockFreq;
float               TicksPerUsec;
//-------------------------------------------------------------

void thhCpuClockInit(void)
{
    TicksPerUsec = (float)(SYSTEM_CLOCK_FREQ / 100000000);
    dbprint("CPU clock runs on %dMHz. system's ticks per uSec = %d", (int)SYSTEM_CLOCK_FREQ, (int)TicksPerUsec);
}
//-------------------------------------------------------------

uint32_t thhGetTicksPerUsec(void)
{
    return TicksPerUsec;
}
//-------------------------------------------------------------

void thhSleep(uint32_t usec2Sleep)
{
   if (usec2Sleep <= 20)
        Task_sleep(1);
    else
        Task_sleep((usec2Sleep - 1) / 10);
}
//-----------------------------------------------------------

void thhDelayUsec(uint32_t usec2Sleep)
{
    while(usec2Sleep--)
        for(uint32_t i = 0; i < NOPS_PER_USEC; i++){MACRO_NOP;}
}
//-----------------------------------------------------------


