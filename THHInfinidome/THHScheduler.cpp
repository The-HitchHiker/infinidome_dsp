/*
 * THHTimers.cpp
 *
 *  Created on: Jul 12, 2021
 *      Author: AmichaiYifrach
 */

#include "THHScheduler.h"
//-----------------------------------------------------------
#ifdef START_WITH_TEST
scheduler_state_t   SchedulerState                          = SCHEDULER_WAITING_FOR_START; //SCHEDULER_GENERAL_TEST_STATE;
#else
scheduler_state_t   SchedulerState                          = SCHEDULER_WAITING_FOR_START;
#endif

bool                dspSleeping                             = true;
bool                dspInited                               = false;
bool                liveGpioPinState                        = false;

Timer_Handle        schedulerTimerHandle;
Semaphore_Handle    schedulerSem;
bool                schedulerTimerInitiated                 = false;
uint32_t            timeFromStart                           = 0;
uint32_t            timeFromMetronom                        = 0;
uint32_t            timeFromMetronomQuart                   = 0;
int                 channelIndex                            = 0;
bool                schedulerRunning                        = false;

Task_Handle         schedulerTask;
Task_Handle         calculationTask;
bool                NeedToClose = false;

uint16_t            adcPattern                              = DEFAULT_ADC_PATTERN;
bool                calcNextCycle = true;
unsigned char       fbNumber;
unsigned char       frequency;
unsigned char       maxAmp;
uint32_t            metronomPeriod                          = SCHEDULER_METRONOM_EVENT_USEC;
uint32_t            metronomPeriodQuart                     = SCHEDULER_METRONOM_QUART_EVENT_USEC;
uint32_t            metronomNextPeriod                      = SCHEDULER_METRONOM_EVENT_USEC;
uint32_t            metronomNextPeriodQuart                 = SCHEDULER_METRONOM_QUART_EVENT_USEC;
uint16_t            pdChannel[4];
rfic_s              Rfic[2][4];
uint32_t            rficSel;

bool                rfChannelEnabled[4]                     = {true, true, true, true};
int                 matrixSize                              = 4;
int                 fpgaMsgLength                           = matrixSize * matrixSize * 120 * 4;

dsp2mcu_spireq_u    Dsp2McuReq;
mcu2dsp_spirep_u    Mcu2DspRep;
dsp2pp_spireq_u     Dsp2PpReq;
pp2dsp_spirep_u     Pp2DspRep[2];

bool tmpBlickLed = true;
//----------------------------------------------------------

void thhSchedulerInit(void)
{
    Error_Block eb;
    Timer_Params timerParams;
    Task_Params taskParams;
    Task_Params calcTaskParams;
    Semaphore_Params semParams;

    Error_init(&eb);
    Semaphore_Params_init(&semParams);
    schedulerSem = Semaphore_create(1, &semParams, &eb);

    Error_init(&eb);
    Timer_Params_init(&timerParams);
    timerParams.period = SCHEDULER_PERIOD_USEC / 8; //(uint32_t)((float)SCHEDULER_PERIOD_USEC / 1.5);
    timerParams.startMode = Timer_StartMode_USER;
    timerParams.periodType = Timer_PeriodType_MICROSECS;
    timerParams.intNum = 13;
    schedulerTimerHandle = Timer_create(Timer_ANY, thhScheduler, &timerParams, &eb);
    if (schedulerTimerHandle == NULL)
    {
        schedulerTimerInitiated = false;
        dbprint("FATAL ERROR: Unable to set Scheduler Timer Handle! aborting...");
        System_abort("FATAL ERROR: Unable to set Scheduler Timer Handle! aborting...");
    }
    else
    {
        schedulerTimerInitiated = true;
    }

    Error_init(&eb);
    Task_Params_init(&taskParams);
    taskParams.stackSize = 4096;
    taskParams.priority = 15;
    schedulerTask = Task_create(thhSchedulerMainThread, &taskParams, &eb);
    if (schedulerTask == NULL)
    {
        dbprint("FATAL ERROR: Unable to set Scheduler Metronom Event Handler! aborting...");
        System_abort("FATAL ERROR: Unable to set Scheduler Metronom Event Handler! aborting...");
    }

    Error_init(&eb);
    Task_Params_init(&calcTaskParams);
    calcTaskParams.stackSize = 4096;
    calcTaskParams.priority = 15;
    calculationTask = Task_create(thhCalcThread, &calcTaskParams, &eb);
    if (calculationTask == NULL)
    {
        dbprint("FATAL ERROR: Unable to set Calculation task! aborting...");
        System_abort("FATAL ERROR: Unable to set Calculation task! aborting...");
    }
}
//---------------------------------------------------------

void thhScheduler(UArg arg0)
{
    if (channelIndex == 0)
    {
        //---- Metronom
//        dbprint("Sending Metronom");
        *GPIO_SET_REG = GPIO_PIN[GPIO_METRONOM_PIN];// | GPIO_PIN[GPIO_ADC_CALIB_RDY_PIN];
        thhDelayUsec(20);
        *GPIO_CLR_REG = GPIO_PIN[GPIO_METRONOM_PIN];// | GPIO_PIN[GPIO_ADC_CALIB_RDY_PIN];
        thhDelayUsec(30);
    }
    else
    {
        thhDelayUsec(50);
    }

    //---- Start Sample
//    dbprint("Sending Start sampling signal");
    *GPIO_SET_REG = GPIO_PIN[GPIO_ADC_START_SMPL_PIN];// | GPIO_PIN[GPIO_ADC_CALIB_RDY_PIN];
    thhDelayUsec(20);
    *GPIO_CLR_REG = GPIO_PIN[GPIO_ADC_START_SMPL_PIN];// | GPIO_PIN[GPIO_ADC_CALIB_RDY_PIN];
    channelIndex++;
    if (channelIndex == 4)
        channelIndex = 0;

    Semaphore_post(schedulerSem);
}
//---------------------------------------------------------

int thhSetScheduler(bool State)
{
    if (schedulerTimerHandle == NULL)
    {
        dbprint("WARNING: trying to set Scheduler state while unavailable");
        return -1;
    }

    if (State)
    {
        if (schedulerRunning)
        {
            dbprint("Stopping Scheduler");
            Timer_stop(schedulerTimerHandle);
        }
        dbprint("Starting Scheduler");
        timeFromMetronom = 0;
        timeFromMetronomQuart = 0;
        timeFromStart = 0;
        channelIndex = 0;
        schedulerRunning = true;
        Timer_start(schedulerTimerHandle);
    }
    else
    {
        dbprint("Stopping Scheduler");
        Timer_stop(schedulerTimerHandle);
        schedulerRunning = false;
        timeFromMetronom = 0;
        timeFromMetronomQuart = 0;
        channelIndex = 0;
    }

    return 0;
}
//-----------------------------------------------------------

int thhResetScheduler(void)
{
    if (schedulerTimerHandle == NULL)
    {
        dbprint("WARNING: trying to reset Scheduler while unavailable");
        return -1;
    }

    thhSetScheduler(SCHEDULER_STOP);
    timeFromMetronom = 0;
    timeFromStart = 0;
    channelIndex = 0;
    thhSetScheduler(SCHEDULER_START);

    return 0;
}
//-----------------------------------------------------------

void thhSchedulerMainThread(UArg arg0, UArg arg1)
{
    uint8_t             SrioBfrsAddrs[12];
    uint8_t             SrioBfrsAddrsV[12];
    bool                SrioAddrsSet = false;
    uint32_t            SrioSetRetries = 100;

    uint32_t            srioBfrSelectAddrsBE;
    uint32_t            srioRxBfr1AddrsBE;
    uint32_t            srioRxBfr0AddrsBE;

    uint32_t            *tmpRet;

    dbprint("Scheduler main thread started");

    while (!NeedToClose)
    {
        switch (SchedulerState)
        {
            case SCHEDULER_GENERAL_TEST_STATE:
                tmpRet = thhSrioGetMsg();
                if (tmpRet != NULL)
                {
                    //dbprint("SRIO message received: %s", HexToStr((unsigned char *)tmpRet, fpgaMsgLength).c_str());
                    SrioSetRetries--;
                    dbprint("SRIO message received: %s", HexToStr((unsigned char *)tmpRet, 256).c_str());
                }
                if (SrioSetRetries > 0)
                    SchedulerState = SCHEDULER_GENERAL_TEST_STATE;
                else
                    thhResetScheduler();
                    SchedulerState = SCHEDULER_ADC_CALIB_INIT;
                break;

            case SCHEDULER_WAITING_FOR_START:
                thhGpioSetPin(GPIO_DSP_READY_PIN, GPIO_SET);
                if (thhGpioGetPin(GPIO_START_STOP_PIN))
                {
                    dbprint("Start signal received, starting state machine");
                    dspSleeping = false;
                    dspInited = false;
#ifdef SRIO_ENABLED
                    SchedulerState = SCHEDULER_SEND_SRIO_PARAM;
#else
                    thhGpioSetPin(GPIO_DSP_READY_PIN, GPIO_SET);
                    thhDelayUsec(SCHEDULER_INIT_ADC_CALIB_EVENT_USEC);
                    SchedulerState = SCHEDULER_ADC_CALIB_INIT;
#endif
                }
                break;

            case SCHEDULER_SEND_SRIO_PARAM:
                dbprint("Sending SRIO params to FPGA");

                srioBfrSelectAddrsBE = (toLittleEndianU32((uint32_t)&srioBfrSelect) | 0x10);
                srioRxBfr1AddrsBE = (toLittleEndianU32((uint32_t)srioRxBfr1) | 0x10);
                srioRxBfr0AddrsBE = (toLittleEndianU32((uint32_t)srioRxBfr0) | 0x10);

                memcpy((uint8_t *)&SrioBfrsAddrs[8], (uint8_t *)&srioBfrSelectAddrsBE, sizeof(uint32_t));
                memcpy((uint8_t *)&SrioBfrsAddrs[4], (uint8_t *)&srioRxBfr0AddrsBE, sizeof(uint32_t));
                memcpy((uint8_t *)&SrioBfrsAddrs[0], (uint8_t *)&srioRxBfr1AddrsBE, sizeof(uint32_t));

                while ((!SrioAddrsSet) && (SrioSetRetries))
                {
                    thhSpiTransaction(DEV_STPW, (void *)SrioBfrsAddrs, (void *)SrioBfrsAddrsV);

                    Task_sleep(1000);

                    thhSpiTransaction(DEV_STPR, (void *)SrioBfrsAddrs, (void *)SrioBfrsAddrsV);

                    if (memcmp((uint8_t *)SrioBfrsAddrs, (uint8_t *)SrioBfrsAddrsV, (sizeof(uint32_t) * 3)) == 0)
                        SrioAddrsSet = true;
                    else
                    {
                        SrioSetRetries--;
                        Task_sleep(1000);
                    }
                }

                if (!SrioAddrsSet)
                {
                    dbprint("FATAL_ERROR: Unable to set srio buffers addresses with FPGA. Aborting");
//                    System_abort("");
                }

                thhSendSrioParamsToMain((uint32_t)&srioBfrSelect, (uint32_t)srioRxBfr1, (uint32_t)srioRxBfr0);

                thhGpioSetPin(GPIO_DSP_READY_PIN, GPIO_SET);
                SrioSetRetries = 10;
#ifdef START_WITH_TEST
                SchedulerState = SCHEDULER_GENERAL_TEST_STATE;
#else
                thhDelayUsec(SCHEDULER_INIT_ADC_CALIB_EVENT_USEC);
                SchedulerState = SCHEDULER_ADC_CALIB_INIT;
#endif
                break;

            case SCHEDULER_ADC_CALIB_INIT:
                if  (thhGpioGetPin(GPIO_ADC_CALIB_REQ_PIN))
                {
                    dbprint("ADC calib init signal received. Initiating ADC calib");

                    Dsp2McuReq.dsp2mcuParser.opCode = SPI_ADC_CALIB_OPCODE;
                    Dsp2McuReq.dsp2mcuParser.Data[0] = (unsigned char)((adcPattern >> 8) & 0xFF);
                    Dsp2McuReq.dsp2mcuParser.Data[1] = (unsigned char)(adcPattern & 0xFF);
                    memset(&Dsp2McuReq.dsp2mcuParser.Data[2], 0x00, SPI_MAIN_BYFF_SIZE - 3);
                    memset(Mcu2DspRep.rxBuf, 0x00, SPI_MAIN_BYFF_SIZE);

                    thhSpiTransaction(DEV_MCU, (void *)&Dsp2McuReq, (void *)&Mcu2DspRep);

                    thhDelayUsec(2000);

                    calcNextCycle = (Mcu2DspRep.rxBuf[0] == 0x01);
                    matrixSize = 0;
                    for (int i = 1; i < 5; i++)
                    {
                        if (Mcu2DspRep.rxBuf[0] & (0x01 << i))
                        {
                            rfChannelEnabled[i] = true;
                            matrixSize++;
                            dbprint("RF Channel #%d enabled", i - 1);
                        }
                        else
                        {
                            rfChannelEnabled[i] = false;
                            dbprint("RF Channel #%d disabled", i - 1);
                        }
                    }
                    fpgaMsgLength = matrixSize * matrixSize * 120 * 4;
                    fbNumber = Mcu2DspRep.mcu2dspParser.fbNumber;
                    frequency = Mcu2DspRep.mcu2dspParser.frequency;
                    maxAmp = Mcu2DspRep.mcu2dspParser.maxAmp;
                    metronomPeriod = (uint32_t)(toLittleEndianU16(Mcu2DspRep.mcu2dspParser.nextMetronomPeriod)) * 1000;
                    metronomPeriodQuart = (uint32_t)(metronomPeriod / 4);
                    Timer_setPeriodMicroSecs(schedulerTimerHandle, metronomPeriodQuart);
                    channelIndex = 0;
                    thhDelayUsec(SCHEDULER_INIT_ADC_CALIB_COMPLETE_USEC);
                    SchedulerState = SCHEDULER_NEED_TO_SEND_ADC_CALIB_COMPLETE;
                }
                else
                {
                    dbprint("ADC calib completed signal received.");
                    SchedulerState = SCHEDULER_SET_NORMAL_OPERATION;
                }
                break;

            case SCHEDULER_NEED_TO_SEND_ADC_CALIB_COMPLETE:
                thhGpioSetPin(GPIO_ADC_CALIB_RDY_PIN, GPIO_SET);
                SchedulerState = SCHEDULER_WAITING_FOR_ADC_CALIB_COMPLETE;
                break;

            case SCHEDULER_WAITING_FOR_ADC_CALIB_COMPLETE:
                if (!thhGpioGetPin(GPIO_ADC_CALIB_REQ_PIN))
                {
                    dbprint("ADC calib completed signal received.");
                    thhGpioSetPin(GPIO_ADC_CALIB_RDY_PIN, GPIO_RESET);
                    thhDelayUsec(2000);
                    SchedulerState = SCHEDULER_SET_NORMAL_OPERATION;
                }
                break;

            case SCHEDULER_SET_NORMAL_OPERATION:
                dbprint("Initiating normal operation");
                thhGpioSetPin(GPIO_ADC_CALIB_RDY_PIN, GPIO_RESET);
                Dsp2McuReq.dsp2mcuParser.opCode = SPI_NORMAL_OP_OPCODE;
                memset(Dsp2McuReq.dsp2mcuParser.Data, 0x00, SPI_MAIN_BYFF_SIZE - 1);
                memset(Mcu2DspRep.rxBuf, 0x00, SPI_MAIN_BYFF_SIZE);

                thhSpiTransaction(DEV_MCU, (void *)&Dsp2McuReq, (void *)&Mcu2DspRep);

                thhDelayUsec(2000);

                calcNextCycle = (Mcu2DspRep.rxBuf[0] == 0x01);
                matrixSize = 0;
                for (int i = 1; i < 5; i++)
                {
                    if (Mcu2DspRep.rxBuf[0] & (0x01 << i))
                    {
                        rfChannelEnabled[i] = true;
                        matrixSize++;
                        dbprint("RF Channel #%d enabled", i - 1);
                    }
                    else
                    {
                        rfChannelEnabled[i] = false;
                        dbprint("RF Channel #%d disabled", i - 1);
                    }
                }
                fpgaMsgLength = matrixSize * matrixSize * 120 * 4;
                fbNumber = Mcu2DspRep.mcu2dspParser.fbNumber;
                frequency = Mcu2DspRep.mcu2dspParser.frequency;
                maxAmp = Mcu2DspRep.mcu2dspParser.maxAmp;
                metronomPeriod = (uint32_t)(toLittleEndianU16(Mcu2DspRep.mcu2dspParser.nextMetronomPeriod)) * 1000;
                metronomPeriodQuart = (uint32_t)(metronomPeriod / 4);
                Timer_setPeriodMicroSecs(schedulerTimerHandle, metronomPeriodQuart);
                channelIndex = 0;
                thhSetScheduler(SCHEDULER_START);
                SchedulerState = SCHEDULER_SEND_WHAT_NEXT;
                break;

            case SCHEDULER_SEND_WHAT_NEXT:
                Semaphore_pend(schedulerSem, BIOS_WAIT_FOREVER);
                //---- What next
                thhDelayUsec(SCHEDULER_REQUEST_WHAT_NEXT_EVENT_USEC);
                dbprint("Sending what next");
                Dsp2McuReq.dsp2mcuParser.opCode = SPI_WHAT_NEXT_OPCODE;
                memset(Dsp2McuReq.dsp2mcuParser.Data, 0x00, SPI_MAIN_BYFF_SIZE - 1);
                memset(Mcu2DspRep.rxBuf, 0x00, SPI_MAIN_BYFF_SIZE);

                thhSpiTransaction(DEV_MCU, (void *)&Dsp2McuReq, (void *)&Mcu2DspRep);

                calcNextCycle = (Mcu2DspRep.rxBuf[0] == 0x01);
                matrixSize = 0;
                for (int i = 1; i < 5; i++)
                {
                    if (Mcu2DspRep.rxBuf[0] & (0x01 << i))
                    {
                        rfChannelEnabled[i] = true;
                        matrixSize++;
                        dbprint("RF Channel #%d enabled", i - 1);
                    }
                    else
                    {
                        rfChannelEnabled[i] = false;
                        dbprint("RF Channel #%d disabled", i - 1);
                    }
                }
                fpgaMsgLength = matrixSize * matrixSize * 120 * 4;
                fbNumber = Mcu2DspRep.mcu2dspParser.fbNumber;
                frequency = Mcu2DspRep.mcu2dspParser.frequency;
                maxAmp = Mcu2DspRep.mcu2dspParser.maxAmp;

                //---- query PP
                for (int i = 0; i < 2; i++)
                {
                    dbprint("Requesting PP%d info", i);

                    Dsp2PpReq.dsp2ppParser.opCode = SPI_SET_RFIC1_OPCODE + i;
                    for (int j = 0; j < 4; j++)
                    {
                        *((uint16_t *)&Dsp2PpReq.dsp2ppParser.Data[j * 4]) = Rfic[rficSel][j].Angle;
                        *((uint16_t *)&Dsp2PpReq.dsp2ppParser.Data[j * 4 + 2]) = Rfic[rficSel][j].Amplitude;
                    }

                    memset(Pp2DspRep[i].rxBuf, 0x00, 17);

                    thhSpiTransaction(DEV_PP0 + i, (void *)&Dsp2PpReq, (void *)&Pp2DspRep);

                    pdChannel[i * 2] = toLittleEndianU16(Pp2DspRep[i].pp2dspParser.pdCh1);
                    pdChannel[i * 2 + 1] = toLittleEndianU16(Pp2DspRep[i].pp2dspParser.pdCh2);
                }
                SchedulerState = SCHEDULER_POLLING_FOR_STOP;
                break;

            case SCHEDULER_POLLING_FOR_STOP:
                dbprint("Polling for STOP signal");
                if (!thhGpioGetPin(GPIO_START_STOP_PIN))
                {
                    thhSetScheduler(SCHEDULER_STOP);
                    dspInited = false;
                    SchedulerState = SCHEDULER_WAITING_FOR_START;
                }
                else

                {
                    SchedulerState = SCHEDULER_SEND_WHAT_NEXT;
                }
                break;

            default:
                break;
        }

    }

    dbprint("Scheduler main thread exited");
}
//------------------------------------------------------------

void thhSendSrioParamsToMain(uint32_t bfrSel, uint32_t bfr1, uint32_t bfr0)
{
    bfrSel |= 0x10000000;
    bfr0 |= 0x10000000;
    bfr1 |= 0x10000000;

    Dsp2McuReq.dsp2mcuParser.opCode = SPI_SRIP_PARAMS_OPCODE;
    Dsp2McuReq.dsp2mcuParser.Data[0] = 0x00;
    memcpy(&Dsp2McuReq.dsp2mcuParser.Data[1], (uint8_t *)&bfr0, sizeof(uint32_t));
    memset(Mcu2DspRep.rxBuf, 0x00, SPI_MAIN_BYFF_SIZE);
    thhSpiTransaction(DEV_MCU, (void *)&Dsp2McuReq, (void *)&Mcu2DspRep);
    thhDelayUsec(20000);

    Dsp2McuReq.dsp2mcuParser.Data[0] = 0x01;
    memcpy(&Dsp2McuReq.dsp2mcuParser.Data[1], (uint8_t *)&bfr1, sizeof(uint32_t));
    memset(Mcu2DspRep.rxBuf, 0x00, SPI_MAIN_BYFF_SIZE);
    thhSpiTransaction(DEV_MCU, (void *)&Dsp2McuReq, (void *)&Mcu2DspRep);
    thhDelayUsec(20000);

    Dsp2McuReq.dsp2mcuParser.Data[0] = 0x02;
    memcpy(&Dsp2McuReq.dsp2mcuParser.Data[1], (uint8_t *)&bfrSel, sizeof(uint32_t));
    memset(Mcu2DspRep.rxBuf, 0x00, SPI_MAIN_BYFF_SIZE);
    thhSpiTransaction(DEV_MCU, (void *)&Dsp2McuReq, (void *)&Mcu2DspRep);
    thhDelayUsec(20000);
}
//------------------------------------------------------------

void thhCalcThread(UArg arg0, UArg arg1)
{
    dbprint("Calculation thread started");

    uint32_t            *tmpRet;

    while (!NeedToClose)
    {
        tmpRet = thhSrioGetMsg();

        if (tmpRet == NULL)
        {
            Task_sleep(1000);
            dbprint("SRIO message wait timed out... retrying");
            continue;
        }

        dbprint("SRIO message received: %s", HexToStr((unsigned char *)tmpRet, 256).c_str());

        if (srioBfrSelect & 0x00000000FFFFFFFF == 0x0000000000000000)
            rficSel = 0;
        else
            rficSel = 1;

        Algo_Run((uint8_t *)tmpRet, Rfic[rficSel]);
    }

    dbprint("Calculation thread exited");
}
//------------------------------------------------------------
