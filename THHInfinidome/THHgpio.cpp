/*
 * THHgpio.c
 *
 *  Created on: Jul 12, 2021
 *      Author: AmichaiYifrach
 */

#include "THHgpio.h"
//----------------------------------------------------------

uint32_t                    GPIO_PIN[32];
uint32_t                    GPIO_PIN_N[32];
//----------------------------------------------------------

void thhGpioSetPinDir(int Pin2Set, bool isInput)
{
    uint32_t tmpDirReg = *GPIO_DIR_REG;

    if (Pin2Set > 15)
    {
        uint32_t tmpPinCtrl = *GPIO_PIN_CONTROL_0_REG;
        tmpPinCtrl |= GPIO_PIN[Pin2Set];
        *GPIO_PIN_CONTROL_0_REG = tmpPinCtrl;
    }

    if (isInput)
    {
        tmpDirReg |= GPIO_PIN[Pin2Set];
        dbprint("Setting GPIO pin %d as INPUT", Pin2Set);
    }
    else
    {
        tmpDirReg &= GPIO_PIN_N[Pin2Set];
        dbprint("Setting GPIO pin %d as OUTPUT", Pin2Set);
    }

    *GPIO_DIR_REG = tmpDirReg;
}
//---------------------------------------------------------

void thhGpioInit(void)
{
    dbprint("Initializing GPIOs...");

    uint32_t tmpPinIndex = 0x00000001;
    for (int i = 0; i < 32; i++)
    {
        GPIO_PIN[i] = tmpPinIndex;
        GPIO_PIN_N[i] = ~tmpPinIndex;
        tmpPinIndex <<= 1;
    }

    // set directions
    thhGpioSetPinDir(GPIO_PP0_CS_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_PP1_CS_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_MCU_CS_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_METRONOM_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_DSP_READY_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_ADC_CALIB_RDY_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_ADC_START_SMPL_PIN, GPIO_OUTPUT_DIR);

    thhGpioSetPinDir(GPIO_LED5_NEAR_FAN_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_LED6_PIN, GPIO_OUTPUT_DIR);

    thhGpioSetPinDir(GPIO_FPGA_STP_WR_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_FPGA_STP_RD_PIN, GPIO_OUTPUT_DIR);

    thhGpioSetPinDir(GPIO_START_STOP_PIN, GPIO_INPUT_DIR);
    thhGpioSetPinDir(GPIO_ADC_CALIB_REQ_PIN, GPIO_INPUT_DIR);

    thhGpioSetPinDir(GPIO_FPGA_SRIO_RST_PIN, GPIO_OUTPUT_DIR);
    thhGpioSetPinDir(GPIO_FPGA_SRIO_RDY_PIN, GPIO_INPUT_DIR);

    thhGpioSetPinDir(GPIO_SRIO_INITIALIZED_PIN, GPIO_INPUT_DIR);

    int unusedOutputs[GPIO_UNUSED_OUTPUTS] = GPIO_UNUSED_OUTPUT_PINS;
    for (int i = 0; i < GPIO_UNUSED_OUTPUTS; i++)
        thhGpioSetPinDir(unusedOutputs[i], GPIO_OUTPUT_DIR);

    int unusedInputs[GPIO_UNUSED_INPUTS] = GPIO_UNUSED_INPUT_PINS;
    for (int i = 0; i < GPIO_UNUSED_INPUTS; i++)
        thhGpioSetPinDir(unusedInputs[i], GPIO_INPUT_DIR);

    // set init values
    thhGpioSetSpiCS(DEV_PP0, GPIO_SET);
    thhGpioSetSpiCS(DEV_PP1, GPIO_SET);
    thhGpioSetSpiCS(DEV_MCU, GPIO_SET);

    thhGpioSetPin(GPIO_METRONOM_PIN, GPIO_RESET);
    thhGpioSetPin(GPIO_ADC_START_SMPL_PIN, GPIO_RESET);
    thhGpioSetPin(GPIO_FPGA_SRIO_RST_PIN, GPIO_RESET);
    thhGpioSetPin(GPIO_LED5_NEAR_FAN_PIN, GPIO_RESET);
    thhGpioSetPin(GPIO_LED6_PIN, GPIO_RESET);

    dbprint("GPIOs Initialized");
}
//---------------------------------------------------------------

void thhGpioSetPin(int Pin2Set, bool State)
{
    uint32_t tmpOutReg = *GPIO_OUT_DATA_REG;

    if (State)
    {
        tmpOutReg |= GPIO_PIN[Pin2Set];
    }
    else
    {
        tmpOutReg &= GPIO_PIN_N[Pin2Set];
    }

    *GPIO_OUT_DATA_REG = tmpOutReg;

    dbprint("Setting GPIO pin %d to %d Success", Pin2Set, (int)State);
}
//---------------------------------------------------------------

bool thhGpioGetPin(int Pin2Get)
{
    uint32_t tmpDirReg = *GPIO_DIR_REG;

    bool isInput = ((tmpDirReg & GPIO_PIN[Pin2Get]) != 0x00000000);
    bool pinState;

    if (isInput)
    {
        uint32_t tmpInReg = *GPIO_IN_DATA_REG;
        pinState = ((tmpInReg & GPIO_PIN[Pin2Get]) != 0x00000000);
        dbprint("Reading GPIO input pin %d as %d", Pin2Get, (int)pinState);
    }
    else
    {
        uint32_t tmpOutReg = *GPIO_OUT_DATA_REG;
        pinState = ((tmpOutReg & GPIO_PIN[Pin2Get]) != 0x00000000);
        dbprint("Reading GPIO output pin %d as %d", Pin2Get, (int)pinState);
    }

    return pinState;
}
//---------------------------------------------------------------

void thhGpioSetSpiCS(device_type_t spiDev, bool State)
{
    switch (spiDev)
    {
        case DEV_PP0:
            dbprint("Setting PP0 SPI CS to %d", (int)State);
            thhGpioSetPin(GPIO_PP0_CS_PIN, State);
            break;

        case DEV_PP1:
            dbprint("Setting PP1 SPI CS to %d", (int)State);
            thhGpioSetPin(GPIO_PP1_CS_PIN, State);
            break;

        case DEV_MCU:
            dbprint("Setting MCU SPI CS to %d", (int)State);
            thhGpioSetPin(GPIO_MCU_CS_PIN, State);
            break;

        default:
            break;
    }
}
//---------------------------------------------------------------

void thhGpioSetMetronom(uint32_t pulseWidthUsec)
{
    *GPIO_SET_REG = GPIO_PIN[GPIO_METRONOM_PIN];
    thhSleep(pulseWidthUsec);
    *GPIO_CLR_REG = GPIO_PIN[GPIO_METRONOM_PIN];
}
//---------------------------------------------------------------
