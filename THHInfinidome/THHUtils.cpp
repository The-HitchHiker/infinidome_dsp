/*
 * THHUtils.cpp
 *
 *  Created on: Aug 12, 2021
 *      Author: AmichaiYifrach
 */

#include "THHUtils.h"
//------------------------------------------------------------------------------

string rtrim(string s, const char* t)
{
    s.erase(s.find_last_not_of(t) + 1);
    return s;
}
//-----------------------------------------------------------

string ltrim(string s, const char* t)
{
    s.erase(0, s.find_first_not_of(t));
    return s;
}
//-----------------------------------------------------------

string trim(string s, const char* t)
{
    return ltrim(rtrim(s, t), t);
}
//-----------------------------------------------------------

string HexToStr(unsigned char *inHex, int Length)
{
    string RetVal = "";
    char tmpCh[2];

    for (int i = 0; i < Length; i++)
    {
        sprintf(tmpCh, "%02X", inHex[i]);
        RetVal += string(tmpCh,2) + " ";
    }

    return trim(RetVal);
}
//---------------------------------------------------------------------------

uint16_t toLittleEndianU16(uint16_t Src)
{
    uint16_t RetVal;
    ((unsigned char *)&RetVal)[0] = ((unsigned char *)&Src)[1];
    ((unsigned char *)&RetVal)[1] = ((unsigned char *)&Src)[0];
    return RetVal;
}
//-----------------------------------------------------------------

uint32_t toLittleEndianU32(uint32_t Src)
{
    uint32_t RetVal;
    ((unsigned char *)&RetVal)[0] = ((unsigned char *)&Src)[3];
    ((unsigned char *)&RetVal)[1] = ((unsigned char *)&Src)[2];
    ((unsigned char *)&RetVal)[2] = ((unsigned char *)&Src)[1];
    ((unsigned char *)&RetVal)[3] = ((unsigned char *)&Src)[0];
    return RetVal;
}
//-----------------------------------------------------------------

