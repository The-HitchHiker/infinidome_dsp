/*
 *  ======== main.c ========
 */
#pragma pack(1)
#include "main.h"


/*
 *  ======== main ========
 */
Int main()
{
    dbprint("Running Infinidome DSP version %s", thhGetVersion());

#ifndef __DEBUG
    void (*x)(void) = _c_int00;

    for(uint32_t i = 0; i< 0xFFFFFF; i++)
    {asm("   NOP");}
#endif  //__DEBUG

    CSL_BootCfgUnlockKicker();

    uint32_t ii;
    uint32_t numConfigs;

    numConfigs = myBoard_getNumPllcConfigs();
    for (ii = 0; ii < numConfigs; ii++)
        init_pll(&mypllcConfigs[ii]);

#ifdef __DEBUG
        for (uint32_t i = 0; i < 0x8000; i++);
#else
        for (uint32_t i = 0; i < 0x80000; i++) {asm("   NOP");}
#endif

    CSL_BootCfgLockKicker();

    Board_initCfg brdCfg;
    brdCfg = /*BOARD_INIT_PLL |*/ BOARD_INIT_MODULE_CLOCK | BOARD_INIT_PINMUX_CONFIG |
                BOARD_INIT_UART_STDIO | BOARD_INIT_DDR | BOARD_INIT_WATCHDOG_DISABLE;
    Board_init(brdCfg);
    thhDelayUsec(10);

    thhCpuClockInit();
    thhDelayUsec(10);

    thhGpioInit();
    thhDelayUsec(10);

    *GPIO_SET_REG = GPIO_PIN[GPIO_LED5_NEAR_FAN_PIN];
    thhDelayUsec(1);
    *GPIO_CLR_REG = GPIO_PIN[GPIO_LED6_PIN];

    thhSpiInit();
    thhDelayUsec(10);
    thhGpioSetPin(GPIO_DSP_READY_PIN, GPIO_SET);

    *GPIO_CLR_REG = GPIO_PIN[GPIO_LED5_NEAR_FAN_PIN];
    thhDelayUsec(1);
    *GPIO_SET_REG = GPIO_PIN[GPIO_LED6_PIN];

#ifdef SRIO_ENABLED
    thhSrioInit();
    thhDelayUsec(10);
#endif

    *GPIO_SET_REG = GPIO_PIN[GPIO_LED5_NEAR_FAN_PIN];
    thhDelayUsec(1);
    *GPIO_SET_REG = GPIO_PIN[GPIO_LED6_PIN];

    Algo_Init();
    thhDelayUsec(10);

    thhSchedulerInit();
    thhDelayUsec(10);

    BIOS_start();    /* does not return */
    return(0);
}
//----------------------------------------------------
