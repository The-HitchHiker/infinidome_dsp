/*
 * THHSrio.h
 *
 *  Created on: Aug 12, 2021
 *      Author: AmichaiYifrach
 */

#ifndef THHSRIO_H_
#define THHSRIO_H_
//--------------------------------------------------

/* SRIO Driver Includes. */
#include <ti/drv/srio/srio_types.h>
#include <ti/drv/srio/include/listlib.h>
#include <ti/drv/srio/srio_drv.h>

/* CSL SRIO Functional Layer */
#include <ti/csl/csl_srio.h>
#include <ti/csl/csl_srioAux.h>
#include <ti/csl/csl_srioAuxPhyLayer.h>

/* CSL BootCfg Module */
#include <ti/csl/csl_bootcfg.h>
#include <ti/csl/csl_bootcfgAux.h>

/* CSL Chip Functional Layer */
#include <ti/csl/csl_chip.h>

/* CSL PSC Module */
#include <ti/csl/csl_pscAux.h>

/* QMSS Include */
#include <ti/drv/qmss/qmss_drv.h>

#include <ti/csl/csl_serdes.h>
#include <ti/csl/csl_serdes_srio.h>

#include "THHGlobalsAndDefs.h"
#include "THHgpio.h"
#include "THHSpi.h"
//----------------------------------------------------------------


#define GARBAGE_LEN_QUEUE           905
#define GARBAGE_TOUT_QUEUE          906
#define GARBAGE_RETRY_QUEUE         907
#define GARBAGE_TRANS_ERR_QUEUE     908
#define GARBAGE_PROG_QUEUE          909
#define GARBAGE_SSIZE_QUEUE         910

#define DEVICE_VENDOR_ID            0x30
#define DEVICE_REVISION             0x0

#define DEVICE_ASSEMBLY_ID          0x0
#define DEVICE_ASSEMBLY_VENDOR_ID   0x30
#define DEVICE_ASSEMBLY_REVISION    0x0
#define DEVICE_ASSEMBLY_INFO        0x0100

#define SRIO_CHANNELS               1
#ifdef __DEBUG
#define SRIO_INIT_TIMEOUT           10
#else
#define SRIO_INIT_TIMEOUT           2
#endif
#define SRIO_GET_NEW_DATA_TIMEOUT   1000
#define SRIO_LINK_STADY_COUNT       10

//-- for loppback test
#define SRIO_ORIG_MSG               0x11221122
#define SRIO_TXED_MSG               0xFEEDFEED

//-----------------------------------------------------------------------


int SrioDevice_init(void);
int enable_srio(void);
void thhSrioInit(void);
void thhSrioTestLoopback(void);
void thhSrioPllConfig();
void thhSrioSerDesConfig();
uint32_t *thhSrioGetMsg(void);


//--------------------------------------------------------------------
#endif /* THHSRIO_H_ */
