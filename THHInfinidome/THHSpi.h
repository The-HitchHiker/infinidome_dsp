/*
 * THHSpi.h
 *
 *  Created on: Jul 12, 2021
 *      Author: AmichaiYifrach
 */

#ifndef THHSPI_H_
#define THHSPI_H_
//----------------------------------------------------------------
#include <ti/drv/spi/SPI.h>
#include <ti/drv/spi/src/v0/SPI_v0.h>
#include <ti/drv/spi/soc/SPI_soc.h>

#include "THHGlobalsAndDefs.h"
#include "THHgpio.h"
//----------------------------------------------------------------

#define SPI_SCLK_FREQ_HZ                    8000000; //12000000
#define SPI_FRAME_SIZE_BITS                 8
#define SPI_FRAME_FORMAT                    SPI_POL0_PHA1
#define SPI_MODE                            SPI_MASTER
#define SPI_TRANSFER_MODE                   SPI_MODE_BLOCKING

#define SPI_PAUSE_BETWEEN_BYTES_USEC        1
#define SPI_HOLD_CS_BEFOR_XFER_USEC         3
#define SPI_HOLD_CS_AFTER_XFER_USEC         1

#define SPI_ADC_CALIB_OPCODE                0x90
#define SPI_NORMAL_OP_OPCODE                0x91
#define SPI_WHAT_NEXT_OPCODE                0x92
#define SPI_SET_RFIC1_OPCODE                0x95
#define SPI_SET_RFIC2_OPCODE                0x96

#define SPI_SRIP_PARAMS_OPCODE              0x99
//-----------------------------------------------------------------

void thhSpiInit(void);
int thhSpiTransaction(device_type_t dstDev, void *txPacket, void *rxPacket);
void thhSendDebugToMain(uint8_t *dbgMsg, uint32_t dbgMsgLength);

//----------------------------------------------------------------
#endif /* THHSPI_H_ */
