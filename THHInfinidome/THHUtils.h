/*
 * THHUtils.h
 *
 *  Created on: Aug 12, 2021
 *      Author: AmichaiYifrach
 */

#ifndef THHUTILS_H_
#define THHUTILS_H_
//----------------------------------------------------------------

#include <stdio.h>
#include <unistd.h>
#include <string>
#include <stdlib.h>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <semaphore.h>
#include <sstream>
#include <algorithm>

using namespace std;
//-----------------------------------------------------------

extern string rtrim(string s, const char* t = " \t\n\r\f\v");
extern string ltrim(string s, const char* t = " \t\n\r\f\v");
extern string trim(string s, const char* t = " \t\n\r\f\v");
string HexToStr(unsigned char *inHex, int Length);
uint16_t toLittleEndianU16(uint16_t Src);
uint32_t toLittleEndianU32(uint32_t Src);

//----------------------------------------------------------------
#endif /* THHUTILS_H_ */
