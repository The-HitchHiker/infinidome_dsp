/*
 * THHSpi.cpp
 *
 *  Created on: Jul 13, 2021
 *      Author: AmichaiYifrach
 */

#include "THHSpi.h"
//-------------------------------------------------------------------

SPI_Params      spiParams;
SPI_Handle      spiHandle;
SPI_v0_HWAttrs  spi_cfg;
SPI_v0_Object   *object = NULL;
//--------------------------------------------------------------

void thhSpiInit(void)
{
    dbprint("Initiating SPI");

    /* Get the default UART init configurations */
    SPI_socGetInitCfg(0, &spi_cfg);
    spi_cfg.enableIntr = false;
    spi_cfg.pinMode = 3;
    SPI_socSetInitCfg(0, &spi_cfg);

    // spi init
    SPI_Params_init(&spiParams);
    spiParams.bitRate = SPI_SCLK_FREQ_HZ;
    spiParams.dataSize = SPI_FRAME_SIZE_BITS;
    spiParams.frameFormat = SPI_FRAME_FORMAT;
    spiParams.mode = SPI_MODE;
    spiParams.transferMode = SPI_TRANSFER_MODE;

    SPI_init();

    spiHandle = (SPI_Handle)SPI_open(0 , &spiParams);
    if (spiHandle == NULL)
    {
        dbprint("FATAL ERROR: Unable to set SPI Handle! aborting...");
        System_abort("");
    }

    uint32_t loop = 0;
    SPI_control(spiHandle,SPI_V0_CMD_LOOPBACK,(void *) &loop);

    /* Enable transfer */
    uint32_t xferEnable = 1;
    SPI_control(spiHandle, SPI_V0_CMD_XFER_ACTIVATE, (void *)&xferEnable);

    dbprint("SPI Initiated successfully");
}
//-------------------------------------------------------------------

int thhSpiTransaction(device_type_t dstDev, void *txPacket, void *rxPacket)
{
    SPI_Transaction transaction;

    if (spiHandle == NULL)
    {
        dbprint("ERROR: Trying to use SPI while not initialized. Ignored...");
        return -1;
    }

    if ((txPacket == NULL) || (rxPacket == NULL))
    {
        dbprint("WARNING: SPI transaction empty. Ignored...");
        return -1;
    }

    dsp2mcu_spireq_u    *Dsp2McuReq     = NULL;
    mcu2dsp_spirep_u    *Mcu2DspRep     = NULL;
    dsp2pp_spireq_u     *Dsp2PpReq      = NULL;
    pp2dsp_spirep_u     *Pp2DspRep      = NULL;
    uint8_t             *tmpTxBuf       = NULL;
    uint8_t             *tmpRxBuf       = NULL;
    int                 tmpXferCount    = 1;
    int                 csPinNumber;

    bool spiRetVal;

    transaction.arg = NULL;

    switch (dstDev)
    {
        case DEV_MCU:
            Dsp2McuReq = (dsp2mcu_spireq_u *)txPacket;
            Mcu2DspRep = (mcu2dsp_spirep_u *)rxPacket;
            tmpTxBuf = (uint8_t *)Dsp2McuReq->txBuf;
            tmpRxBuf = (uint8_t *)Mcu2DspRep->rxBuf;
            tmpXferCount = sizeof(Dsp2McuReq->txBuf);
            csPinNumber = GPIO_MCU_CS_PIN;
            break;

        case DEV_PP0:
            Dsp2PpReq = (dsp2pp_spireq_u *)txPacket;
            Pp2DspRep = (pp2dsp_spirep_u *)rxPacket;
            tmpTxBuf = (uint8_t *)Dsp2PpReq->txBuf;
            tmpRxBuf = (uint8_t *)Pp2DspRep->rxBuf;
            tmpXferCount = sizeof(Dsp2PpReq->txBuf);
            csPinNumber = GPIO_PP0_CS_PIN;
            break;

        case DEV_PP1:
            Dsp2PpReq = (dsp2pp_spireq_u *)txPacket;
            Pp2DspRep = (pp2dsp_spirep_u *)rxPacket;
            tmpTxBuf = (uint8_t *)Dsp2PpReq->txBuf;
            tmpRxBuf = (uint8_t *)Pp2DspRep->rxBuf;
            tmpXferCount = sizeof(Dsp2PpReq->txBuf);
            csPinNumber = GPIO_PP1_CS_PIN;
            break;

        case DEV_STPR:
            tmpTxBuf = (uint8_t *)txPacket;
            tmpRxBuf = (uint8_t *)rxPacket;
            tmpXferCount = 12;
            csPinNumber = GPIO_FPGA_STP_RD_PIN;
            break;

        case DEV_STPW:
            tmpTxBuf = (uint8_t *)txPacket;
            tmpRxBuf = (uint8_t *)rxPacket;
            tmpXferCount = 12;
            csPinNumber = GPIO_FPGA_STP_WR_PIN;
            break;

        default:
            dbprint("WARNING: SPI transaction requested to unknown device. ignored...");
            return -1;
    }

    *GPIO_CLR_REG = GPIO_PIN[csPinNumber];

    thhDelayUsec(SPI_HOLD_CS_BEFOR_XFER_USEC);

//    transaction.txBuf = tmpTxBuf;
//    transaction.rxBuf = tmpRxBuf;
//    transaction.count = tmpXferCount;
//
//
//    spiRetVal = SPI_transfer(spiHandle, &transaction);
//    if (!spiRetVal)
//    {
//        dbprint("SPI transaction with device %d failed. frames transfered: %d", (int)dstDev, (int)transaction.count);
//        return -1;
//    }

    for (int i = 0; i < tmpXferCount; i++)
    {
        transaction.txBuf = &tmpTxBuf[i];
        transaction.rxBuf = &tmpRxBuf[i];
        transaction.count = 1;

        spiRetVal = SPI_transfer(spiHandle, &transaction);

        tmpRxBuf[i] = ((uint8_t *)transaction.rxBuf)[0];

        thhDelayUsec(SPI_PAUSE_BETWEEN_BYTES_USEC);

        if (!spiRetVal)
        {
            dbprint("SPI transaction with device %d failed. frames transfered: %d", (int)dstDev, (int)transaction.count);
            return -1;
        }
    }

    thhDelayUsec(SPI_HOLD_CS_AFTER_XFER_USEC);

    *GPIO_SET_REG = GPIO_PIN[csPinNumber];

    dbprint("SPI transaction successful. \r\n\tTx=\"%s\"\r\n\tRx=\"%s\"", HexToStr(tmpTxBuf, tmpXferCount).c_str(), HexToStr(tmpRxBuf, tmpXferCount).c_str());

    return 0;
}
//-------------------------------------------------------------------


void thhSendDebugToMain(uint8_t *dbgMsg, uint32_t dbgMsgLength)
{
    dsp2mcu_spireq_u    Dsp2McuReq;
    mcu2dsp_spirep_u    Mcu2DspRep;

    memset(Dsp2McuReq.txBuf, 0x00, SPI_MAIN_BYFF_SIZE);
    Dsp2McuReq.dsp2mcuParser.opCode = SPI_SRIP_PARAMS_OPCODE;
    Dsp2McuReq.dsp2mcuParser.Data[0] = 0x80;
    memcpy(&Dsp2McuReq.dsp2mcuParser.Data[1], dbgMsg, dbgMsgLength);
    memset(Mcu2DspRep.rxBuf, 0x00, SPI_MAIN_BYFF_SIZE);
    thhSpiTransaction(DEV_MCU, (void *)&Dsp2McuReq, (void *)&Mcu2DspRep);
    thhDelayUsec(20000);
}
//------------------------------------------------------------
