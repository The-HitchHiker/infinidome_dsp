/*
 * THHAlgo.h
 *
 *  Created on: Sep 13, 2021
 *      Author: The HitchHiker
 */

#ifndef THHALGO_H_
#define THHALGO_H_

#define CALCK 1

//----------------------------------------------

#include "THHGlobalsAndDefs.h"
#include "THHgpio.h"
//--------------------------------------------

#define MY_PI 3.141592653589793

#define N 4
#define L_FFT 2048
#define N_FRAME 122

#ifdef CALCK
void eigen3x3(float C[3][3], int *max_idx);
void joint_diag(float jthresh);//(float *a[N], float *ib[N], float jthresh, float V_r[N][N], float V_i[N][N]);
#endif

#ifdef _THH_Algo_CPP_
    float *rx_save_a[N];
    float *rx_save_ib[N];
    uint8_t ch_Available_LUT[N] = {1,1,1,1};
#else
    extern float *rx_save_a[N];
    extern float *rx_save_ib[N];
    extern uint8_t ch_Available_LUT[N];
#endif

void div_cmplx(float a, float b, float c, float d, float *r_r, float *r_i);

uint8_t         Algo_Init(void);
uint8_t         Algo_Run(uint8_t *selectedBuffer, rfic_s *selectedRfic);



//---------------------------------------------
#endif /* THHALGO_H_ */
