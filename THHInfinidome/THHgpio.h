/*
 * THHgpio.h
 *
 *  Created on: Jul 12, 2021
 *      Author: AmichaiYifrach
 */

#ifndef THHGPIO_H_
#define THHGPIO_H_
//-------------------------------------------------------------

#include "THHGlobalsAndDefs.h"
#include "THHClockAndTimers.h"
//--------------------------------------------------------------

#define GPIO_BASE                   0x02320000
#define GPIO_BINTEN_REG             (uint32_t *)(GPIO_BASE + 0x0008)
#define GPIO_DIR_REG                (uint32_t *)(GPIO_BASE + 0x0010)
#define GPIO_OUT_DATA_REG           (uint32_t *)(GPIO_BASE + 0x0014)
#define GPIO_IN_DATA_REG            (uint32_t *)(GPIO_BASE + 0x0020)
#define GPIO_SET_REG                (uint32_t *)(GPIO_BASE + 0x0018)
#define GPIO_CLR_REG                (uint32_t *)(GPIO_BASE + 0x001C)
#define GPIO_SET_RIS_TRIG_REG       (uint32_t *)(GPIO_BASE + 0x0024)
#define GPIO_CLR_RIS_TRIG_REG       (uint32_t *)(GPIO_BASE + 0x0028)

#define GPIO_PIN_CONTROL_0_REG      ((uint32_t *)0x02620580)

#define GPIO_SET                    true
#define GPIO_RESET                  false

#define GPIO_INPUT_DIR              true
#define GPIO_OUTPUT_DIR             false


#ifdef __EVM6657

#define GPIO_PP0_CS_PIN             19
#define GPIO_PP1_CS_PIN             18
#define GPIO_MCU_CS_PIN             25
#define GPIO_METRONOM_PIN           27
#define GPIO_START_STOP_PIN         24
#define GPIO_ADC_CALIB_REQ_PIN      26
#define GPIO_DSP_READY_PIN          21

#define GPIO_LED5_PIN               20
#define GPIO_LED6_PIN               21
#define GPIO_LED7_PIN               22
#define GPIO_LED8_PIN               23

#define GPIO_A0_PIN                 15
#define GPIO_A1_PIN                 14

#else

#define GPIO_PP0_CS_PIN             17
#define GPIO_PP1_CS_PIN             17  //TODO: Need tobe change from the LED
#define GPIO_MCU_CS_PIN             16
#define GPIO_METRONOM_PIN           19
#define GPIO_START_STOP_PIN         0
#define GPIO_ADC_CALIB_REQ_PIN      1
#define GPIO_ADC_CALIB_RDY_PIN      18
#define GPIO_DSP_READY_PIN          22
#define GPIO_LED5_NEAR_FAN_PIN      15
#define GPIO_LED6_PIN               14
#define GPIO_SRIO_INITIALIZED_PIN   10
#define GPIO_ADC_START_SMPL_PIN     23
#define GPIO_FPGA_STP_WR_PIN        27
#define GPIO_FPGA_STP_RD_PIN        26
#define GPIO_FPGA_SRIO_RDY_PIN      21
#define GPIO_FPGA_SRIO_RST_PIN      20

#define GPIO_UNUSED_OUTPUTS         1
#define GPIO_UNUSED_OUTPUT_PINS     {22}

#define GPIO_UNUSED_INPUTS          15
#define GPIO_UNUSED_INPUT_PINS      {2,3,4,5,6,7,8,9,11,12,13,24,25,28,29}

//#define GPIO_A0_PIN                 1
//#define GPIO_A1_PIN                 2

#endif
//--------------------------------------------------------------


void thhGpioInit(void);
void thhGpioSetPinDir(int Pin2Set, bool isInput = true);
void thhGpioSetPin(int Pin2Set, bool State = GPIO_SET);
bool thhGpioGetPin(int Pin2Get);
void thhGpioSetSpiCS(device_type_t spiDev, bool State);
void thhGpioSetMetronom(uint32_t pulseWidthUsec);
void thhGpioSetRisTrig(int Pin2Set, bool State);
//----------------------------------------------------------------




//-------------------------------------------------------------
#endif /* THHGPIO_H_ */
