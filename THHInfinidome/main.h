/*
 * main.h
 *
 *  Created on: Jul 12, 2021
 *      Author: AmichaiYifrach
 */

#ifndef MAIN_H_
#define MAIN_H_
//-----------------------------------------------------

#include "THHGlobalsAndDefs.h"
#include "THHHardwareStartup.h"
#include "THHVersionHistory.h"
#include "THHgpio.h"
#include "THHScheduler.h"
#include "THHAlgo.h"
//---------------------------------------------------------


#endif /* MAIN_H_ */
