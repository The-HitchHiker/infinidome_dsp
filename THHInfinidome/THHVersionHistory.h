/*
 * THHVersionHistory.h
 *
 *  Created on: Jul 18, 2021
 *      Author: AmichaiYifrach
 */

#ifndef THHVERSIONHISTORY_H_
#define THHVERSIONHISTORY_H_
//-------------------------------------------------------------------------

#include "THHGlobalsAndDefs.h"
//---------------------------------------------------------------------------

#define VERSION                         0
// 0 : Development
//---------------------------------------------------------------------

#define BUILD                           0
// 0 : Pre Build
//---------------------------------------------------------------------

#define REVISION                        20
// 20   :   merge algo into project, update uSecDalay function, add spi debug with main, fix srio startup, fix PLL config
// 19   :   change bss to algo
// 18   :   SPI set to 3wires, set timers and test metronom, quad, what next etc timing ,
//          fix SRIO initialization with FPGA, add calc function waiting on SRIO
// 17   :   SRIO integration test, HWstartup sequence added
// 16   :   SRIO buffers addresses sent to FPGA by SPI
// 15   :   SPI with main + state machine verified
// 14   :   update gpio allocation and signals according to DSP-FPGA - IO interface pinout connection.xlsx, temp disable SRIO,
//      :   added output signals to FPGA:calib complete & start sample, Image creation post-build script added
// 13   :   better flaxibility in SRIO cfg
// 12   :   SRIO PLL & SerDes config by defines, added thhSrioGetMsg using double bfr
// 11   :   UART1 pins set as Input, added active rf channels cmd at startup
// 10   :   SRIO SerDes config switch added to thhSrioInit
// 9    :   added SRIO link (loopback works, normal mode hangs on EVM - need to test in integration), added DSP_READY signal to FPGA, added SPI transaction debug print
// 8    :   GCh [04.08.2021]: change enum types and definitions (strong type)
// 7    :   SPI working.
// 6    :   Change GPIO set/get to LUT, add default to cases statements, removed race condition possibility in gpio and the protection (for speed), adjust thhSleep
// 5    :   GPIO for evm OK
// 4    :   ifdef __EVM6657 for GPIO, restructured scheduler
// 3    :   changed StartStop to polling, fix thhSleep
// 2    :   added startup sequence (no sleep mode yet)
// 1    :   same as 13.07.21.1422
//---------------------------------------------------------------------

char tmpVersion[255];

char *thhGetVersion(void)
{
    memset(tmpVersion, 0x00, 255);

    sprintf(tmpVersion, "%d.%d.%d", VERSION, BUILD, REVISION);

    return tmpVersion;
}
//-----------------------------------------------------

//------------------------------------------------------------------------
#endif /* THHVERSIONHISTORY_H_ */
