/*
 * THHHardwareStartup.h
 *
 *  Created on: Sep 26, 2021
 *      Author: The HitchHiker
 */

#ifndef THHHARDWARESTARTUP_H_
#define THHHARDWARESTARTUP_H_
//---------------------------------------------------------

#include "THHGlobalsAndDefs.h"
//---------------------------------------------------------

uint8_t     g_u8SpiOneTxBuff[10];
uint8_t     g_u8SpiOneRxBuff[10];

extern      void _c_int00(void);

uint8_t     Flash_Release_PowerDown_ReadDeviceID(uint8_t u8Cmnd);
void        NorFlashWriteRegCmnd(uint8_t u8Cmnd);
uint8_t     FlashReadChipId(uint8_t u8Cmnd, uint8_t* pB);
uint32_t    myBoard_getNumPllcConfigs();
static void pllc_delay(uint32_t loops);
static void wait_for_completion(const pllcConfig *data);
void        init_pll(const pllcConfig *data);
//------------------------------------------------------------------




#endif /* THHHARDWARESTARTUP_H_ */
