/*
 * THHHardwareStartup.cpp
 *
 *  Created on: Sep 26, 2021
 *      Author: The HitchHiker
 */

#include "THHHardwareStartup.h"
//------------------------------------------------------------------

static CSL_PllcHandle pllcReg = (CSL_PllcHandle)PLLCTL_REGS_BASE_ADDR;

const pllcConfig mypllcConfigs[] = {
    {CSL_PLL_SYS,    20,     1/*1*/,    2/*2*/}, /* 1000 MHz */
    {CSL_PLL_DDR3,   80,     3,         2/*2*/}, /* 666.66 MHz*/
};
//------------------------------------------------------------------

uint32_t myBoard_getNumPllcConfigs()
{
    return (sizeof(mypllcConfigs) / sizeof(mypllcConfigs));
}
//------------------------------------------------------------------

static void pllc_delay(uint32_t loops)
{
    while (loops--)
    {
        asm("   NOP");
    }
}
//------------------------------------------------------------------

static void wait_for_completion(const pllcConfig *data)
{
    int i;
    for (i = 0; i < 100; i++)
    {
        pllc_delay(450);
        if ((pllctl_reg_read(PLLSTAT) & PLLSTAT_GO) == 0)
            break;
    }
}
//------------------------------------------------------------------

void init_pll(const pllcConfig *data)
{
    uint32_t tmp, tmp_ctl, pllm, plld, pllod, bwadj;

    pllm = data->pll_m - 1;
    plld = (data->pll_d - 1) & PLL_DIV_MASK;
    pllod = (data->pll_od - 1) & PLL_CLKOD_MASK;

    if (data->pll == CSL_PLL_SYS)
    {
        /* The requered delay before main PLL configuration */
        pllc_delay(210000);

        tmp = pllctl_reg_read(SECCTL);

        if (tmp & (PLLCTL_BYPASS))
        {
            setbits32(keystonePllcRegs[data->pll].reg1,
                     PLLCTL_ENSAT);

            pllctl_reg_clrbits(PLLCTL, PLLCTL_PLLEN |
                       PLLCTL_PLLENSRC);
            pllc_delay(340);

            pllctl_reg_setbits(SECCTL, PLLCTL_BYPASS);
            pllctl_reg_setbits(PLLCTL, PLLCTL_PLLPWRDN);
            pllc_delay(21000);

            pllctl_reg_clrbits(PLLCTL, PLLCTL_PLLPWRDN);
        }
        else
        {
            pllctl_reg_clrbits(PLLCTL, PLLCTL_PLLEN |
                       PLLCTL_PLLENSRC);
            pllc_delay(340);
        }

        pllctl_reg_write(PLLM, pllm & PLLM_MULT_LO_MASK);

        clrsetbits32(keystonePllcRegs[data->pll].reg0,
                PLLM_MULT_HI_SMASK, (pllm << 6));

        /* Set the BWADJ     (12 bit field)  */
        tmp_ctl = pllm >> 1; /* Divide the pllm by 2 */
        clrsetbits32(keystonePllcRegs[data->pll].reg0,
                PLL_BWADJ_LO_SMASK,
                (tmp_ctl << PLL_BWADJ_LO_SHIFT));
        clrsetbits32(keystonePllcRegs[data->pll].reg1,
                PLL_BWADJ_HI_MASK,
                (tmp_ctl >> 8));

        /*
         * Set the pll divider (6 bit field) *
         * PLLD[5:0] is located in MAINPLLCTL0
         */
        clrsetbits32(keystonePllcRegs[data->pll].reg0,
                PLL_DIV_MASK, plld);

        /* Set the OUTPUT DIVIDE (4 bit field) in SECCTL */
        pllctl_reg_rmw(SECCTL, PLL_CLKOD_SMASK,
                   (pllod << PLL_CLKOD_SHIFT));
        wait_for_completion(data);

        pllctl_reg_write(PLLDIV1_3[0], PLLM_RATIO_DIV1);
        pllctl_reg_write(PLLDIV1_3[1], PLLM_RATIO_DIV2);
        pllctl_reg_write(PLLDIV1_3[2], PLLM_RATIO_DIV3);
        pllctl_reg_write(PLLDIV4_16[0], PLLM_RATIO_DIV4);
        pllctl_reg_write(PLLDIV4_16[1], PLLM_RATIO_DIV5);

        pllctl_reg_setbits(ALNCTL, 0x1f);

        /*
         * Set GOSET bit in PLLCMD to initiate the GO operation
         * to change the divide
         */
        pllctl_reg_setbits(PLLCMD, PLLSTAT_GO);
        pllc_delay(1500); /* wait for the phase adj */
        wait_for_completion(data);


        /* Reset PLL */
        pllctl_reg_setbits(PLLCTL, PLLCTL_PLLRST);
        pllc_delay(21000);  /* Wait for a minimum of 7 us*/
        pllctl_reg_clrbits(PLLCTL, PLLCTL_PLLRST);
        pllc_delay(105000); /* Wait for PLL Lock time (min 50 us) */

        pllctl_reg_clrbits(SECCTL, PLLCTL_BYPASS);

        pllctl_reg_setbits(PLLCTL, PLLCTL_PLLEN);


    }
    else
    {
        setbits32(keystonePllcRegs[data->pll].reg1, PLLCTL_ENSAT);
        /*
         * process keeps state of Bypass bit while programming
         * all other DDR PLL settings
         */
        tmp = HW_RD_REG32(keystonePllcRegs[data->pll].reg0);
        tmp &= PLLCTL_BYPASS;   /* clear everything except Bypass */
        setbits32(keystonePllcRegs[data->pll].reg0, PLLCTL_BYPASS);

        /*
         * Set the BWADJ[7:0], PLLD[5:0] and PLLM to PLLCTL0,
         * bypass disabled
         */
        bwadj = pllm >> 1;
        tmp |= ((bwadj & PLL_BWADJ_LO_MASK) << PLL_BWADJ_LO_SHIFT) |
            (pllm << PLL_MULT_SHIFT) |
            (plld & PLL_DIV_MASK) |
            (pllod << PLL_CLKOD_SHIFT);
        HW_WR_REG32(keystonePllcRegs[data->pll].reg0, tmp);

        /* Set BWADJ[11:8] bits */
        tmp = HW_RD_REG32(keystonePllcRegs[data->pll].reg1);
        tmp &= ~(PLL_BWADJ_HI_MASK);
        tmp |= ((bwadj >> 8) & PLL_BWADJ_HI_MASK);

        HW_WR_REG32(keystonePllcRegs[data->pll].reg1, tmp);

        /* Reset bit: bit 14 for both DDR3 & PASS PLL */
        tmp = PLL_PLLRST;
        /* Set RESET bit = 1 */
        setbits32(keystonePllcRegs[data->pll].reg1, tmp);
        /* Wait for a minimum of 7 us*/
        pllc_delay(21000);
        /* Clear RESET bit */
        clrbits32(keystonePllcRegs[data->pll].reg1, tmp);
        pllc_delay(105000);

        /* clear BYPASS (Enable PLL Mode) */
        clrbits32(keystonePllcRegs[data->pll].reg0, PLLCTL_BYPASS);
        pllc_delay(21000);  /* Wait for a minimum of 7 us*/
    }

    /*
     * This is required to provide a delay between multiple
     * consequent PPL configurations
     */
    pllc_delay(210000);
}
//------------------------------------------------------------------



