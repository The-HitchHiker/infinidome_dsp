/*
 * THHGlobalsAndDefs.h
 *
 *  Created on: Jul 12, 2021
 *      Author: AmichaiYifrach
 */

#ifndef THHGLOBALSANDDEFS_H_
#define THHGLOBALSANDDEFS_H_
//------------------------------------------------------------------------------

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Types.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/gates/GateHwi.h>
#include <ti/sysbios/timers/timer64/Timer.h>
#include <xdc/cfg/global.h>

#ifdef __cplusplus
extern "C" {
#include <ti/mathlib/mathlib.h>
#include <ti/dsplib/dsplib.h>
}
#endif

#include "keystone_pll.h"

#include <c6x.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>

#include <ti/board/board.h>

#include "THHUtils.h"
//--------------------------------------------------------------------------

#ifdef __DEBUG
#define dbprint(x, ...) {time_t now = time(NULL); System_printf("[%s %s:%s line:%d DEBUG:] " x "\r\n" ,ctime(&now), __FILE__, __func__,__LINE__,##__VA_ARGS__);System_flush();}
#else
#define dbprint(x, ...)
#endif
//--------------------------------------------------------------------------

typedef unsigned char device_type_t;
#define DEV_PP0                             0x01
#define DEV_PP1                             0x02
#define DEV_MCU                             0x03
#define DEV_STPR                            0x04
#define DEV_STPW                            0x05

#define CLK_PER_USEC                        40

#define SPI_MAIN_BYFF_SIZE                  6
#define SPI_PP_BYFF_SIZE                    17

#define MACRO_NOP                           asm("    NOP")
//-------------------------------------------------------------------

union dsp2mcu_spireq_u
{
    struct dsp2mcu_spireq_s
    {
        unsigned char       opCode;
        unsigned char       Data[SPI_MAIN_BYFF_SIZE - 1];
    }dsp2mcuParser;
    unsigned char txBuf[SPI_MAIN_BYFF_SIZE];
};
//--------------

union mcu2dsp_spirep_u
{
    struct mcu2dsp_spirep_s
    {
        unsigned char       calcNext;
        unsigned char       fbNumber;
        unsigned char       frequency;
        unsigned char       maxAmp;
        uint16_t            nextMetronomPeriod;
    }mcu2dspParser;
    unsigned char rxBuf[SPI_MAIN_BYFF_SIZE];
};
//--------------

union dsp2pp_spireq_u
{
    struct dsp2pp_spireq_s
    {
        unsigned char       opCode;
        unsigned char       Data[SPI_PP_BYFF_SIZE - 1];
    }dsp2ppParser;
    unsigned char txBuf[SPI_PP_BYFF_SIZE];
};
//--------------

union pp2dsp_spirep_u
{
    struct pp2dsp_spirep_s
    {
        unsigned char       opCode;
        uint16_t            pdCh1;
        uint16_t            pdCh2;
        unsigned char       dummyData[SPI_PP_BYFF_SIZE - 5];
    }pp2dspParser;
    unsigned char rxBuf[SPI_PP_BYFF_SIZE];
};
//--------------

struct rfic_s
{
    uint16_t            Angle;
    uint16_t            Amplitude;
};
//---------------

extern char tmpVersion[255];

extern const pllcConfig mypllcConfigs[];

extern float            TicksPerUsec;

extern uint16_t         adcPattern;
extern bool             calcNextCycle;
extern unsigned char    fbNumber;
extern unsigned char    frequency;
extern unsigned char    maxAmp;
extern uint32_t         metronomNextPeriod;
extern uint32_t         metronomNextPeriodQuart;
extern uint16_t         pdChannel[4];
extern rfic_s           Rfic[2][4];
extern bool             rfChannelEnabled[4];
extern int              matrixSize;
extern uint32_t         GPIO_PIN[32];
extern uint32_t         GPIO_PIN_N[32];

extern uint64_t srioBfrSelect;
extern uint8_t srioRxBfr1[0x10000];
extern uint8_t srioRxBfr0[0x10000];

//---------------------------------------------------------------------
#endif /* THHGLOBALSANDDEFS_H_ */
