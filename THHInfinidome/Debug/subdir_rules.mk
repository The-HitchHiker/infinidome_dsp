################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
%.obj: ../%.cpp $(GEN_OPTS) | $(GEN_FILES) $(GEN_MISC_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: C6000 Compiler'
	"C:/ti/ccs1040/ccs/tools/compiler/ti-cgt-c6000_8.3.11/bin/cl6x" -mv6600 --include_path="C:/ti/dsplib_c66x_3_4_0_4/packages" --include_path="C:/ti/mathlib_c66x_3_1_2_4/packages" --include_path="C:/ti/pdk_c665x_2_0_16/packages/ti/board/src/evmC6657/include" --include_path="C:/ti/pdk_c665x_2_0_16/packages/ti/board/src/evmKeystone" --include_path="C:/ti/pdk_c665x_2_0_16/packages/ti/board/src/evmKeystone/include" --include_path="C:/ti/pdk_c665x_2_0_16/packages/ti/board/src/evmC6657" --include_path="I:/Documents/Aymop/infinidome/SW/InfinidomeWorkspace/THHInfinidome" --include_path="C:/ti/bios_6_76_03_01/packages/ti/posix/ccs" --include_path="C:/ti/ccs1040/ccs/tools/compiler/ti-cgt-c6000_8.3.11/include" --define=SRIO_ENABLED --define=NSRIO_LOOPBACK --define=N__EVM6657 --define=NSTART_WITH_TEST --define=SOC_C6657 --define=__DEBUG -g --c99 --rtti --cpp_default --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="$(basename $(<F)).d_raw" --include_path="I:/Documents/Aymop/infinidome/SW/InfinidomeWorkspace/THHInfinidome/Debug/syscfg" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

build-19979809:
	@$(MAKE) --no-print-directory -Onone -f subdir_rules.mk build-19979809-inproc

build-19979809-inproc: ../app.cfg
	@echo 'Building file: "$<"'
	@echo 'Invoking: XDCtools'
	"C:/ti/ccs1040/xdctools_3_62_01_15_core/xs" --xdcpath="C:/ti/bios_6_76_03_01/packages;C:/ti/pdk_c665x_2_0_16/packages;C:/ti/openmp_dsp_c665x_2_06_03_00/packages;C:/ti/framework_components_3_40_02_07/packages;C:/ti/framework_components_3_40_02_07/examples;C:/ti/edma3_lld_2_12_05_30E/packages;C:/ti/ctoolslib_2_2_0_0/packages;" xdc.tools.configuro -o configPkg -t ti.targets.elf.C66 -p ti.platforms.evm6657 -r release -c "C:/ti/ccs1040/ccs/tools/compiler/ti-cgt-c6000_8.3.11" --compileOptions "-g" "$<"
	@echo 'Finished building: "$<"'
	@echo ' '

configPkg/linker.cmd: build-19979809 ../app.cfg
configPkg/compiler.opt: build-19979809
configPkg/: build-19979809


