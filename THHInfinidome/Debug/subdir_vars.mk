################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CFG_SRCS += \
../app.cfg 

CPP_SRCS += \
../THHAlgo.cpp \
../THHClockAndTimers.cpp \
../THHHardwareStartup.cpp \
../THHScheduler.cpp \
../THHSpi.cpp \
../THHSrio.cpp \
../THHUtils.cpp \
../THHgpio.cpp \
../main.cpp 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_MISC_DIRS += \
./configPkg/ 

GEN_OPTS += \
./configPkg/compiler.opt 

OBJS += \
./THHAlgo.obj \
./THHClockAndTimers.obj \
./THHHardwareStartup.obj \
./THHScheduler.obj \
./THHSpi.obj \
./THHSrio.obj \
./THHUtils.obj \
./THHgpio.obj \
./main.obj 

CPP_DEPS += \
./THHAlgo.d \
./THHClockAndTimers.d \
./THHHardwareStartup.d \
./THHScheduler.d \
./THHSpi.d \
./THHSrio.d \
./THHUtils.d \
./THHgpio.d \
./main.d 

GEN_MISC_DIRS__QUOTED += \
"configPkg\" 

OBJS__QUOTED += \
"THHAlgo.obj" \
"THHClockAndTimers.obj" \
"THHHardwareStartup.obj" \
"THHScheduler.obj" \
"THHSpi.obj" \
"THHSrio.obj" \
"THHUtils.obj" \
"THHgpio.obj" \
"main.obj" 

CPP_DEPS__QUOTED += \
"THHAlgo.d" \
"THHClockAndTimers.d" \
"THHHardwareStartup.d" \
"THHScheduler.d" \
"THHSpi.d" \
"THHSrio.d" \
"THHUtils.d" \
"THHgpio.d" \
"main.d" 

GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 

CPP_SRCS__QUOTED += \
"../THHAlgo.cpp" \
"../THHClockAndTimers.cpp" \
"../THHHardwareStartup.cpp" \
"../THHScheduler.cpp" \
"../THHSpi.cpp" \
"../THHSrio.cpp" \
"../THHUtils.cpp" \
"../THHgpio.cpp" \
"../main.cpp" 


