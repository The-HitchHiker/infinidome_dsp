/*
 * THHSrio.cpp
 *
 *  Created on: Aug 12, 2021
 *      Author: AmichaiYifrach
 */
#include "THHSrio.h"
//--------------------------------------------

CSL_SrioHandle hSrio;

uint64_t srioBfrSelect = 0xFFFFFFFFFFFFFFFF;
uint8_t srioRxBfr1[0x10000];
uint8_t srioRxBfr0[0x10000];

uint64_t lastBufferSelect = 0xFFFFFFFFFFFFFFFF;

SRIO_PE_FEATURES    peFeatures;
SRIO_OP_CAR         opCar;
Qmss_QueueHnd       queueHnd;
uint8_t             isAllocated;
uint32_t            gargbageQueue[] = { GARBAGE_LEN_QUEUE,  GARBAGE_TOUT_QUEUE,
                                        GARBAGE_RETRY_QUEUE,GARBAGE_TRANS_ERR_QUEUE,
                                        GARBAGE_PROG_QUEUE, GARBAGE_SSIZE_QUEUE };

const uint32_t DEVICE_ID1_16BIT    = 0xBEEF;
const uint32_t DEVICE_ID1_8BIT     = 0xAB;
const uint32_t DEVICE_ID2_16BIT    = 0x4560;
const uint32_t DEVICE_ID2_8BIT     = 0xCD;

uint32_t destination;
uint32_t source;
int LSU = 0;

uint8_t srioDbg[4];
//-----------------------------------------------

int SrioDevice_init(void)
{
    //CSL_SrioHandle      hSrio;
    int32_t             i;
    SRIO_PE_FEATURES    peFeatures;
    SRIO_OP_CAR         opCar;

    memset(srioRxBfr0, 0xFF, sizeof(srioRxBfr0));
    memset(srioRxBfr1, 0xFF, sizeof(srioRxBfr1));
    memset((uint8_t *)&srioBfrSelect, 0xFF, sizeof(srioBfrSelect));

//    srioDbg[0] = 0x01;
//    thhSendDebugToMain(srioDbg, 1);
    /* Get the CSL SRIO Handle. */
    hSrio = CSL_SRIO_Open(0);
    if (hSrio == NULL)
    {
        dbprint("FATAL ERROR: Unable to set SRIO Handle! aborting...");
        return -1;
    }

    /* Code to disable SRIO reset isolation */
    if (CSL_PSC_isModuleResetIsolationEnabled(CSL_PSC_LPSC_SRIO))
        CSL_PSC_disableModuleResetIsolation(CSL_PSC_LPSC_SRIO);

    /* Disable the SRIO Global block */
    CSL_SRIO_GlobalDisable (hSrio);

    /* Disable each of the individual SRIO blocks. */
    for(i = 0; i <= 9; i++)
        CSL_SRIO_DisableBlock(hSrio, i);

    /* Set boot complete to be 0; we are not done with the initialization. */
    CSL_SRIO_SetBootComplete(hSrio, 0);

    /* Now enable the SRIO block and all the individual blocks also. */
    CSL_SRIO_GlobalEnable (hSrio);
    for(i = 0; i <= 9; i++)
        CSL_SRIO_EnableBlock(hSrio,i);

#ifdef SRIO_LOOPBACK
    /* Configure SRIO ports to operate in loopback mode. */
    CSL_SRIO_SetLoopbackMode(hSrio, 0);
    CSL_SRIO_SetLoopbackMode(hSrio, 1);
    CSL_SRIO_SetLoopbackMode(hSrio, 2);
    CSL_SRIO_SetLoopbackMode(hSrio, 3);
#else
//    /* Configure SRIO ports to operate in normal mode. */
    CSL_SRIO_SetNormalMode(hSrio, 0);
    CSL_SRIO_SetNormalMode(hSrio, 1);
    CSL_SRIO_SetNormalMode(hSrio, 2);
    CSL_SRIO_SetNormalMode(hSrio, 3);
#endif


    /* Enable Automatic Priority Promotion of response packets. */
    CSL_SRIO_EnableAutomaticPriorityPromotion(hSrio);

    /* Set the SRIO Prescalar select to operate in the range of 44.7 to 89.5 */
    CSL_SRIO_SetPrescalarSelect (hSrio, 0);

    /* Unlock the Boot Configuration Kicker */
    CSL_BootCfgUnlockKicker();

    thhSrioPllConfig();
    thhSrioSerDesConfig();

//    srioDbg[0] = 0x02;
//    thhSendDebugToMain(srioDbg, 1);

    /* Loop around till the SERDES PLL is not locked. */
    uint32_t status;
    uint32_t retries = 10000;
    while (retries)
    {
        /* Get the SRIO SERDES Status */
        CSL_BootCfgGetSRIOSERDESStatus(&status);
        if (status & 0x1)
            break;
        retries--;
    }

    //Remark by Yoav
    if ((!retries) && (!(status & 0x1)))
    {
        dbprint("FATAL ERROR: Unable to unlock SRIO SERDES PLL! aborting...");
        return -1;
    }

    /* Clear the LSU pending interrupts. */
    CSL_SRIO_ClearLSUPendingInterrupt (hSrio, 0xFFFFFFFF, 0xFFFFFFFF);

    /* Set the Device Information */
    CSL_SRIO_SetDeviceInfo(hSrio, DEVICE_ID1_16BIT, DEVICE_VENDOR_ID, DEVICE_REVISION);

    /* Set the Assembly Information */
    CSL_SRIO_SetAssemblyInfo(hSrio, DEVICE_ASSEMBLY_ID, DEVICE_ASSEMBLY_VENDOR_ID,
                             DEVICE_ASSEMBLY_REVISION, DEVICE_ASSEMBLY_INFO);

    /* TODO: Configure the processing element features
     *  The SRIO RL file is missing the Re-transmit Suppression Support (Bit6) field definition */
    peFeatures.isBridge                          = 0;
    peFeatures.isEndpoint                        = 0;
    peFeatures.isProcessor                       = 1;
    peFeatures.isSwitch                          = 0;
    peFeatures.isMultiport                       = 0;
    peFeatures.isFlowArbiterationSupported       = 0;
    peFeatures.isMulticastSupported              = 0;
    peFeatures.isExtendedRouteConfigSupported    = 0;
    peFeatures.isStandardRouteConfigSupported    = 1;
    peFeatures.isFlowControlSupported            = 1;
    peFeatures.isCRFSupported                    = 0;
    peFeatures.isCTLSSupported                   = 1;
    peFeatures.isExtendedFeaturePtrValid         = 1;
    peFeatures.numAddressBitSupported            = 1;
    CSL_SRIO_SetProcessingElementFeatures(hSrio, &peFeatures);

    /* Configure the source operation CAR */
    memset ((void *) &opCar, 0, sizeof (opCar));
    opCar.portWriteOperationSupport = 1;
    opCar.atomicClearSupport        = 1;
    opCar.atomicSetSupport          = 1;
    opCar.atomicDecSupport          = 1;
    opCar.atomicIncSupport          = 1;
    opCar.atomicTestSwapSupport     = 1;
    opCar.doorbellSupport           = 1;
    opCar.dataMessageSupport        = 1;
    opCar.writeResponseSupport      = 1;
    opCar.streamWriteSupport        = 1;
    opCar.writeSupport              = 1;
    opCar.readSupport               = 1;
    opCar.dataStreamingSupport      = 1;
    CSL_SRIO_SetSourceOperationCAR(hSrio, &opCar);

    /* Configure the destination operation CAR */
    memset ((void *) &opCar, 0, sizeof (opCar));
    opCar.portWriteOperationSupport  = 1;
    opCar.doorbellSupport            = 1;
    opCar.dataMessageSupport         = 1;
    opCar.writeResponseSupport       = 1;
    opCar.streamWriteSupport         = 1;
    opCar.writeSupport               = 1;
    opCar.readSupport                = 1;
    CSL_SRIO_SetDestOperationCAR(hSrio, &opCar);

    /* Set the 16 bit and 8 bit identifier for the SRIO Device. */
    CSL_SRIO_SetDeviceIDCSR(hSrio, DEVICE_ID1_8BIT, DEVICE_ID1_16BIT);

    /* Enable TLM Base Routing Information for Maintainance Requests & ensure that
     * the BRR's can be used by all the ports. */
    CSL_SRIO_SetTLMPortBaseRoutingInfo(hSrio, 0, 1, 1, 1, 0);
    CSL_SRIO_SetTLMPortBaseRoutingInfo(hSrio, 0, 2, 1, 1, 0);
    CSL_SRIO_SetTLMPortBaseRoutingInfo(hSrio, 0, 3, 1, 1, 0);
    CSL_SRIO_SetTLMPortBaseRoutingInfo(hSrio, 1, 0, 1, 1, 0);

    /* Configure the Base Routing Register to ensure that all packets matching the
     * Device Identifier & the Secondary Device Id are admitted. */
    CSL_SRIO_SetTLMPortBaseRoutingPatternMatch(hSrio, 0, 1, DEVICE_ID2_16BIT, 0xFFFF);
    CSL_SRIO_SetTLMPortBaseRoutingPatternMatch(hSrio, 1, 0, DEVICE_ID2_8BIT,  0xFF);

    /* Set the Host Device Identifier. */
    CSL_SRIO_SetHostDeviceID(hSrio, DEVICE_ID1_16BIT);

    /* Configure the component tag CSR */
    CSL_SRIO_SetCompTagCSR(hSrio, 0x00000000);

    /* Configure the PLM for all the ports. */
    for (i = 0; i < 4; i++)
    {
        /* Set the PLM Port Silence Timer. */
        CSL_SRIO_SetPLMPortSilenceTimer(hSrio, i, 0x2);

        /* TODO: We need to ensure that the Port 0 is configured to support both
         * the 2x and 4x modes. The Port Width field is read only. So here we simply
         * ensure that the Input and Output ports are enabled. */
        CSL_SRIO_EnableInputPort(hSrio, i);
        CSL_SRIO_EnableOutputPort(hSrio, i);

        /* Set the PLM Port Discovery Timer. */
        CSL_SRIO_SetPLMPortDiscoveryTimer(hSrio, i, 0x2);

        /* Reset the Port Write Reception capture. */
        CSL_SRIO_SetPortWriteReceptionCapture(hSrio, i, 0x0);
    }

    /* Set the Port link timeout CSR */
    CSL_SRIO_SetPortLinkTimeoutCSR(hSrio, 0x000FFF);
    CSL_SRIO_SetPortResponseTimeoutCSR(hSrio, 0xFF0FFF);

    /* Set the Port General CSR: Only executing as Master Enable */
    CSL_SRIO_SetPortGeneralCSR(hSrio, 0, 1, 0);

    /* Clear the sticky register bits. */
    CSL_SRIO_SetLLMResetControl(hSrio, 1);

    /* Set the device id to be 0 for the Maintenance Port-Write operation
     * to report errors to a system host. */
    CSL_SRIO_SetPortWriteDeviceId(hSrio, 0x0, 0x0, 0x0);

    /* Set the Data Streaming MTU */
    CSL_SRIO_SetDataStreamingMTU(hSrio, 64);

    /* Configure the path mode for the ports. */
    for(i = 0; i < 4; i++)
        CSL_SRIO_SetPLMPortPathControlMode(hSrio, i, 0);
//    CSL_SRIO_SetPLMPortPathControlMode(hSrio, 0, 4);

    /* Set the LLM Port IP Prescalar. */
    CSL_SRIO_SetLLMPortIPPrescalar(hSrio, 0x21);

    /* Enable the peripheral. */
    CSL_SRIO_EnablePeripheral(hSrio);

    /* Configuration has been completed. */
    CSL_SRIO_SetBootComplete(hSrio, 1);

//    *GPIO_SET_REG = GPIO_PIN[GPIO_ADC_CALIB_RDY_PIN];
    /* This code checks if the ports are operational or not. The functionality is not supported
     * on the simulator. */
//    srioDbg[0] = 0x03;
//    thhSendDebugToMain(srioDbg, 1);

    uint16_t linkRetry;
    bool portOK = false;
    for(i = 0; i < SRIO_CHANNELS; i++)
    {
        linkRetry = 20;

        while ((!(portOK = (CSL_SRIO_IsPortOk(hSrio, i) == 1U/*TRUE*/))) && (linkRetry))
        {
            thhDelayUsec(10000);
            memcpy(&srioDbg[1], (uint8_t *)&linkRetry, sizeof(uint16_t));
//            srioDbg[3] = (uint8_t)i;
//            thhSendDebugToMain(srioDbg, sizeof(uint16_t) + 2);
            linkRetry--;
        }

        if ((linkRetry == 0) && (!portOK))
        {
            dbprint("FATAL ERROR: Unable to init SRIO port %d! aborting...", i);
            return -1;
        }
//        memcpy(&srioDbg[1], (uint8_t *)&linkRetry, sizeof(uint16_t));
//        srioDbg[3] = i;
//        thhSendDebugToMain(srioDbg, sizeof(uint16_t) + 2);
    }

#ifdef SRIO_LOOPBACK
    /* Set all the queues 0 to operate at the same priority level and to send packets onto Port 0 */
    for (i =0 ; i < 16; i++)
        CSL_SRIO_SetTxQueueSchedInfo(hSrio, i, 0, 0);

    /* Set the Doorbell route to determine which routing table is to be used
     * This configuration implies that the Interrupt Routing Table is configured as
     * follows:-
     *  Interrupt Destination 0 - INTDST 16
     *  Interrupt Destination 1 - INTDST 17
     *  Interrupt Destination 2 - INTDST 18
     *  Interrupt Destination 3 - INTDST 19
     */
    CSL_SRIO_SetDoorbellRoute(hSrio, 0);

    /* Route the Doorbell interrupts.
     *  Doorbell Register 0 - All 16 Doorbits are routed to Interrupt Destination 0.
     *  Doorbell Register 1 - All 16 Doorbits are routed to Interrupt Destination 1.
     *  Doorbell Register 2 - All 16 Doorbits are routed to Interrupt Destination 2.
     *  Doorbell Register 3 - All 16 Doorbits are routed to Interrupt Destination 3. */
    for (i = 0; i < 16; i++)
    {
        CSL_SRIO_RouteDoorbellInterrupts(hSrio, 0, i, 0);
        CSL_SRIO_RouteDoorbellInterrupts(hSrio, 1, i, 1);
        CSL_SRIO_RouteDoorbellInterrupts(hSrio, 2, i, 2);
        CSL_SRIO_RouteDoorbellInterrupts(hSrio, 3, i, 3);
    }
#endif

    /* Initialization has been completed. */
    return 0;
}
//------------------------------------------------------------------------

int enable_srio(void)
{
    /* SRIO power domain is turned OFF by default. It needs to be turned on before doing any
     * SRIO device register access. This not required for the simulator. */

    /* Set SRIO Power domain to ON */
    CSL_PSC_enablePowerDomain (CSL_PSC_PD_SRIO);

    /* Enable the clocks too for SRIO */
    CSL_PSC_setModuleNextState (CSL_PSC_LPSC_SRIO, PSC_MODSTATE_ENABLE);

    /* Start the state transition */
    CSL_PSC_startStateTransition (CSL_PSC_PD_SRIO);

    /* Wait until the state transition process is completed. */
    uint32_t retries = 10000;
    while ((!CSL_PSC_isStateTransitionDone (CSL_PSC_PD_SRIO)) && (retries--))
    {
        thhDelayUsec(40000);
    }

    if ((retries == 0) && (!CSL_PSC_isStateTransitionDone(CSL_PSC_PD_SRIO)))
    {
        dbprint("FATAL ERROR: Unable to complete SRIO state transition process! aborting...");
        return -1;
    }

    /* Return SRIO PSC status */
    if ((CSL_PSC_getPowerDomainState(CSL_PSC_PD_SRIO) == PSC_PDSTATE_ON) &&
        (CSL_PSC_getModuleState (CSL_PSC_LPSC_SRIO) == PSC_MODSTATE_ENABLE))
    {
        /* SRIO ON. Ready for use */
        return 0;
    }
    else
    {
        /* SRIO Power on failed. Return error */
        return -1;
    }
}
//------------------------------------------------------------------------------

void thhSrioInit(void)
{
    /* Power on SRIO peripheral before using it */
    if (enable_srio() < 0)
    {
        dbprint("FATAL ERROR: SRIO PSC Initialization Failed! aborting...");
        //System_abort(""); //YOAV Comment
    }

    /* Device Specific SRIO Initializations: This should always be called before
     * initializing the SRIO Driver. */
    uint32_t srioRetry = 0x1000;
    bool srioOK = false;
    while ((srioRetry) && (!srioOK))
    {
        srioOK = (SrioDevice_init() == 0);

        if (!srioOK)
        {
            srioRetry--;
            *GPIO_SET_REG = GPIO_PIN[GPIO_FPGA_SRIO_RST_PIN];
            thhDelayUsec(10);
            *GPIO_CLR_REG = GPIO_PIN[GPIO_FPGA_SRIO_RST_PIN];
            srioDbg[0] = 0xEE;
            thhSendDebugToMain(srioDbg, 1);


        }
        thhDelayUsec(550);
    }

//    if (!srioOK)
//    {
//        System_abort("");
//    }

    srioBfrSelect = lastBufferSelect;

    thhSrioTestLoopback();

    /* SRIO Driver is operational at this time. */
    dbprint("SRIO Driver has been initialized");
}
//------------------------------------------------------------------------------

void thhSrioTestLoopback(void)
{
    destination = (uint32_t)srioRxBfr0;
    source = (uint32_t)srioRxBfr1;
    LSU = 0;

#ifndef SRIO_LOOPBACK
    return;
#endif

    *((uint32_t *)destination) =  SRIO_ORIG_MSG;
    *((uint32_t *)source)    =  SRIO_TXED_MSG;
    dbprint("Destination address = 0x%x, Source address = 0x%x\n", destination, source);
    dbprint("Before SRIO transaction, destination value = 0x%x, source value = 0x%x\n",*((uint32_t *)destination), *((uint32_t *)source));

    /* Make sure there is space in the Shadow registers to write*/
    time_t Timeout = false;
    time_t startTime = time(NULL);
    while ((CSL_SRIO_IsLSUFull (hSrio, LSU) != 0) && (!Timeout))
    {
        //Timeout = (difftime(time(NULL), startTime) >= SRIO_INIT_TIMEOUT);
        for(uint32_t i = 0; i<200000; i++){asm("    NOP");}
    }

    if ((Timeout) && (CSL_SRIO_IsLSUFull(hSrio, LSU) != 0))
    {
        dbprint("TEST FAILED: there is NO space in the Shadow registers to write! aborting...");
        return;
    }

    CSL_SRIO_SetLSUReg0 (hSrio, LSU, 0); //no rapidio MSB
    CSL_SRIO_SetLSUReg1 (hSrio, LSU, destination);
    CSL_SRIO_SetLSUReg2 (hSrio, LSU, source);
    CSL_SRIO_SetLSUReg3 (hSrio, LSU, 0x4, 0); //write 4 bytes, no doorbell
    CSL_SRIO_SetLSUReg4 (hSrio, LSU,
        0x4560,         // destid = 0x4560
        0,      // src id map = 0, using RIO_DEVICEID_REG0
        1,      // id size = 1 for 16bit device IDs
        0,      // outport id = 0
        0,      // priority = 0
        0,      // xambs = 0
        0,      // suppress good interrupt = 0 (don't care about interrupts)
        0);     // interrupt request = 0
    CSL_SRIO_SetLSUReg5 (hSrio, LSU,
        4,      // ttype,
        5,      // ftype,
        0,      // hop count = 0,
        0);     // doorbell = 0

    Timeout = false;
    startTime = time(NULL);
    while ((*((int32_t *)destination) != *((int32_t *)source)) && (!Timeout))
    {
        //Timeout = (difftime(time(NULL), startTime) >= SRIO_INIT_TIMEOUT);
        for(uint32_t i = 0; i<200000; i++){asm("    NOP");}
    }

    if ((Timeout) && (*((int32_t *)destination) != *((int32_t *)source)))
    {
        dbprint("TEST FAILED: SRIO transaction timed out...");
        return;
    }
    dbprint("After SRIO transaction, destination value = 0x%x, source value = 0x%x\n",*((uint32_t *)destination), *((uint32_t *)source));
}
//----------------------------------------------------------------------------

void thhSrioPllConfig()
{
    uint32_t pllConfig;
    uint32_t loopBandwidth;
    uint32_t vcoSpeed;
    uint32_t mpy;

#ifdef SRIO_LANE_RATE_5P000GBPS
    loopBandwidth = (0x000000000 << 11);
    vcoSpeed = (0x00000000 << 9);
    mpy = (0x00000028 << 1);
#else
    loopBandwidth = (0x00000000 << 11);
    vcoSpeed = (0x00000001 << 9);
    mpy = (0x00000028 << 1);
#endif
    /* program the PLL */
    pllConfig = (loopBandwidth | vcoSpeed | mpy | 0x0001);
    CSL_BootCfgSetSRIOSERDESConfigPLL(pllConfig);

    dbprint("Setting SRIO CFGPLL register to 0x%08x", pllConfig);
}
//-----------------------------------------------------------------------------------

void thhSrioSerDesConfig()
{
    uint32_t rxConfig = 0x00000000;
    uint32_t rxTESTPTRN = 0x00;
    uint32_t rxLOOPBACK = 0x00;
    uint32_t rxENOC = 0x01;
    uint32_t rxEQHLD = 0x00;
    uint32_t rxEQ = 0x01;
    uint32_t rxCDR = 0x00;
    uint32_t rxLOS = 0x00;
    uint32_t rxALIGN = 0x01;
    uint32_t rxTERM = 0x01;
    uint32_t rxINVPAIR = 0x00;
    uint32_t rxRATE = 0x02;
    uint32_t rxBUSWIDTH = 0x02;
    uint32_t rxEN = 0x01;

    uint32_t txConfig = 0x00000000;
    uint32_t txTESTPTRN = 0x00;
    uint32_t txLOOPBACK = 0x00;
    uint32_t txMSYNC = 0x01;
    uint32_t txFIRUPT = 0x01;
    uint32_t txTWPST1 = 0x00;
    uint32_t txTWPRE = 0x00;
    uint32_t txSWING = 0x0F;
    uint32_t txINVPAIR = 0x00;
    uint32_t txRATE = 0x02;
    uint32_t txBUSWIDTH = 0x02;
    uint32_t txEN = 0x01;

#ifdef SRIO_LANE_RATE_5P000GBPS // 5Gbps
    rxTESTPTRN = 0x00;
    rxENOC = 0x01;
    rxEQHLD = 0x00;
    rxEQ = 0x01;
    rxCDR = 0x00;
    rxLOS = 0x00;
    rxALIGN = 0x01;
    rxTERM = 0x01;
    rxINVPAIR = 0x00;
    rxRATE = 0x01;
    rxBUSWIDTH = 0x02;

    txTESTPTRN = 0x00;
    txLOOPBACK = 0x00;
    txMSYNC = 0x01;
    txFIRUPT = 0x01;
    txTWPST1 = 0x00;
    txTWPRE = 0x00;
    txSWING = 0x0F;
    txINVPAIR = 0x00;
    txRATE = 0x01;
    txBUSWIDTH = 0x02;
    txEN = 0x01;
#else // 2.5Gbps
    rxTESTPTRN = 0x00;
    rxENOC = 0x01;
    rxEQHLD = 0x00;
    rxEQ = 0x01;
    rxCDR = 0x00;
    rxLOS = 0x00;
    rxALIGN = 0x01;
    rxTERM = 0x01;
    rxINVPAIR = 0x00;
    rxRATE = 0x02;
    rxBUSWIDTH = 0x02;

    txTESTPTRN = 0x00;
    txLOOPBACK = 0x00;
    txMSYNC = 0x01;
    txFIRUPT = 0x01;
    txTWPST1 = 0x00;
    txTWPRE = 0x00;
    txSWING = 0x0F;
    txINVPAIR = 0x00;
    txRATE = 0x02;
    txBUSWIDTH = 0x02;
    txEN = 0x01;
#endif

#ifdef SRIO_LOOPBACK
    rxLOOPBACK = 0x03;
    txLOOPBACK = 0x03;
#endif

    rxConfig |= (rxTESTPTRN << 25);
    rxConfig |= (rxLOOPBACK << 23);
    rxConfig |= (rxENOC << 22);
    rxConfig |= (rxEQHLD << 21);
    rxConfig |= (rxEQ << 18);
    rxConfig |= (rxCDR << 15);
    rxConfig |= (rxLOS << 12);
    rxConfig |= (rxALIGN << 10);
    rxConfig |= (rxTERM << 7);
    rxConfig |= (rxINVPAIR << 6);
    rxConfig |= (rxRATE << 4);
    rxConfig |= (rxBUSWIDTH << 1);
    rxConfig |= (rxEN);

    txConfig |= (txTESTPTRN << 23);
    txConfig |= (txLOOPBACK << 21);
    txConfig |= (txMSYNC << 20);
    txConfig |= (txFIRUPT << 19);
    txConfig |= (txTWPST1 << 14);
    txConfig |= (txTWPRE << 11);
    txConfig |= (txSWING << 7);
    txConfig |= (txINVPAIR << 6);
    txConfig |= (txRATE << 4);
    txConfig |= (txBUSWIDTH << 1);
    txConfig |= (txEN);

    /* Configure the SRIO SERDES Receive Configuration. */
    CSL_BootCfgSetSRIOSERDESRxConfig (0, rxConfig);
    CSL_BootCfgSetSRIOSERDESRxConfig (1, rxConfig);
    CSL_BootCfgSetSRIOSERDESRxConfig (2, rxConfig);
    CSL_BootCfgSetSRIOSERDESRxConfig (3, rxConfig);

    /* Configure the SRIO SERDES Transmit Configuration. */
    CSL_BootCfgSetSRIOSERDESTxConfig (0, txConfig);
    CSL_BootCfgSetSRIOSERDESTxConfig (1, txConfig);
    CSL_BootCfgSetSRIOSERDESTxConfig (2, txConfig);
    CSL_BootCfgSetSRIOSERDESTxConfig (3, txConfig);

    dbprint("Setting SRIO SerDesRx registers to 0x%08x", rxConfig);
    dbprint("Setting SRIO SerDesTx registers to 0x%08x", txConfig);
}
//----------------------------------------------------------------------------

uint32_t *thhSrioGetMsg(void)
{
    uint32_t *retBfrPtr = NULL;
    uint32_t startTick = Clock_getTicks();
    bool Timeout = false;
    while ((srioBfrSelect == lastBufferSelect) && (!Timeout))
    {
        Timeout = ((Clock_getTicks() - startTick) >= SRIO_GET_NEW_DATA_TIMEOUT);
    }

    if ((Timeout) && (srioBfrSelect == lastBufferSelect))
        return NULL;

    lastBufferSelect = srioBfrSelect;

    if (srioBfrSelect & 0x00000000FFFFFFFF == 0x0000000000000000)
        retBfrPtr = ((uint32_t *)srioRxBfr0);
    else
        retBfrPtr = ((uint32_t *)srioRxBfr1);

    return retBfrPtr;
}
//----------------------------------------------------------------------------
