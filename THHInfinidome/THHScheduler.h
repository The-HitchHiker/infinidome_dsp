/*
 * THHTimers.h
 *
 *  Created on: Jul 12, 2021
 *      Author: AmichaiYifrach
 */

#ifndef THHSCHEDULER_H_
#define THHSCHEDULER_H_
//-----------------------------------------------------------

#include "THHGlobalsAndDefs.h"
#include "THHgpio.h"
#include "THHSpi.h"
#include "THHSrio.h"
#include "THHAlgo.h"
//------------------------------------------------------------

#define SCHEDULER_PERIOD_USEC                       1000000
#define SCHEDULER_MAX_USEC                          10000000
#define METRONOM_PULSE_WIDTH_USEC                   20
#define SCHEDULER_METRONOM_EVENT_USEC               1000000
#define SCHEDULER_METRONOM_QUART_EVENT_USEC         (int)(SCHEDULER_METRONOM_EVENT_USEC/4)
#define SCHEDULER_SAMPLE_START_EVENT_USEC           50
#define SCHEDULER_REQUEST_WHAT_NEXT_EVENT_USEC      600
#define SCHEDULER_REQUEST_PPS_EVENT_USEC            620
#define SCHEDULER_INIT_ADC_CALIB_EVENT_USEC         50000
#define SCHEDULER_INIT_ADC_CALIB_COMPLETE_USEC      5000
#define SCHEDULER_INIT_TIMEOUT_EVENT_USEC           10000000
#define DEFAULT_ADC_PATTERN                         0x0011

#define SCHEDULER_STOP                              false
#define SCHEDULER_START                             true

typedef enum{
    SCHEDULER_WAITING_FOR_START = 0x00,
    SCHEDULER_ADC_CALIB_INIT,//                 = 0x01,
    SCHEDULER_NEED_TO_SEND_ADC_CALIB_COMPLETE,//= 0x02,
    SCHEDULER_WAITING_FOR_ADC_CALIB_COMPLETE,// = 0x03,
    SCHEDULER_ADC_CALIB_COMPLETE,//             = 0x04,
    SCHEDULER_SET_NORMAL_OPERATION,//           = 0x05,
    SCHEDULER_SEND_METRONOM,//                  = 0x06,
    SCHEDULER_SAMPLE_START,//                   = 0x07,
    SCHEDULER_SEND_WHAT_NEXT,//                 = 0x08,
    SCHEDULER_QUERY_PPS,//                      = 0x09,
    SCHEDULER_WAITING_FOR_QUAD,//               = 0x0A,
    SCHEDULER_POLLING_FOR_STOP,//               = 0x0B,
    SCHEDULER_SEND_SRIO_PARAM,//                = 0x0C

    SCHEDULER_GENERAL_TEST_STATE                = 0xF1
}scheduler_state_t;

//------------------------------------------------------------

void thhSchedulerInit(void);
void thhScheduler(UArg arg0);
int thhSetScheduler(bool State);
int thhResetScheduler(void);

void thhSchedulerMainThread(UArg arg0, UArg arg1);
void thhSendSrioParamsToMain(uint32_t bfrSel, uint32_t bfr1, uint32_t bfr0);
void thhCalcThread(UArg arg0, UArg arg1);
//------------------------------------------------------------------



//------------------------------------------------------------
#endif /* THHSCHEDULER_H_ */
