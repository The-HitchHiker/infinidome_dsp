/*
 * THHClockAndTimers.h
 *
 *  Created on: Jul 13, 2021
 *      Author: AmichaiYifrach
 */

#ifndef THHCLOCKANDTIMERS_H_
#define THHCLOCKANDTIMERS_H_
//--------------------------------------------------------

#include "THHGlobalsAndDefs.h"
//---------------------------------------------------------

#define SYSTEM_CLOCK_FREQ               100000000
#ifdef __DEBUG
#define NOPS_PER_USEC                   12
#else
#define NOPS_PER_USEC                   122
#endif
//------------------------------------------------------------

void thhCpuClockInit(void);
uint32_t thhGetTicksPerUsec(void);
void thhSleep(uint32_t usec2Sleep);
void thhDelayUsec(uint32_t usec2Sleep);

//------------------------------------------------------
#endif /* THHCLOCKANDTIMERS_H_ */
