/*
 * THHAlgo.cpp
 *
 *  Created on: Sep 13, 2021
 *      Author: The HitchHiker
 */

#define _THH_Algo_CPP_      1

#include "THHAlgo.h"

float eivec[3][3];
//---------------------------------------------------------

bool BlickLed = true;

uint8_t Algo_Init(void)
{



    return 0;
}
//------------------------------------------------------

uint8_t Algo_Run(uint8_t *selectedBuffer, rfic_s *selectedRfic)
{
/////////////////////////////////




    int i, k, r, c;

    float V_r[N][N];
    float V_i[N][N];


    UInt32 start_t, end_t;

    float *p_xfr = (float *)selectedBuffer;

    for (i = 0; i < N; i++)
    {
        rx_save_a[i] = (float*)calloc(N*L_FFT, sizeof(float));
        rx_save_ib[i] = (float*)calloc(N*L_FFT, sizeof(float));
    }
#ifdef TEST_BSS
    for (i =0; i<16; i++)
    {
        printf("%f,  ", p_xfr[i]);
    }
    printf("\n");
    for (i =0; i<16; i++)
    {
        printf("%f,  ", p_xfr[121 * (2*N * L_FFT) + i]);
    }
    printf("\n");
#endif


    int bin, frame;
    for (frame=0; frame<N_FRAME; frame++)
    {
        for (bin=0; bin<50; bin++)
        {
            for (r=0; r<N; r++)
            {
                    p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-r)*2+1] = 0;
                    p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-r)*2] = 0;
            }
        }
    }

    for (frame=0; frame<N_FRAME; frame++)
    {
        for (bin=450; bin<L_FFT; bin++)
        {
            for (r=0; r<N; r++)
            {
                    p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-r)*2+1] = 0;
                    p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-r)*2] = 0;
            }
        }
    }


    start_t = Clock_getTicks();
    for (bin = 0; bin < L_FFT; bin++)
    {
        //GCh matrix calculation
        for (r = 0; r < N; r++)
        {
            if(ch_Available_LUT[r] == 0)
                continue;

            for (c = 0; c < N; c++)
            {
                if(ch_Available_LUT[c] == 0)
                    continue;

                for (frame = 0; frame < N_FRAME; frame++)
                {
                    //rx_save_a[r][c+N*bin] += a[frame][bin][r] * a[frame][bin][c] + ib[frame][bin][r] * ib[frame][bin][c];
                    rx_save_a[r][N*bin+c] += p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-r)*2+1] *
                                             p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-c)*2+1] +
                                             p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-r)*2] *
                                             p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-c)*2];
                    //rx_save_ib[r][c+N*bin] += -a[frame][bin][r] * ib[frame][bin][c] + ib[frame][bin][r] * a[frame][bin][c];
                    rx_save_ib[r][c+N*bin] += -p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-r)*2+1] *
                                               p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-c)*2] +
                                               p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-r)*2] *
                                               p_xfr[frame * (2*N * L_FFT) + (N*2) * bin + (3-c)*2+1];
                }
                rx_save_a[r][c+N*bin] /= (float)N_FRAME;
                rx_save_ib[r][c+N*bin] /= (float)N_FRAME;
            }
        }
    }
    end_t = Clock_getTicks();
//    printf("\n Time autocorr %d ticks\n", end_t - start_t);

    start_t = Clock_getTicks();
    joint_diag(75e-11); //joint_diag(rx_save_a, rx_save_ib, 75e-11, V_r, V_i);

    end_t = Clock_getTicks();
//    printf("\n Time joint_diag %d ticks\n", end_t - start_t);

#ifdef TEST_BSS

    printf("V =>\n");
    for (r=0; r<N; r++)
    {
        for (c=0; c<N; c++)
            printf("%1.4f %1.4fi   ",V_r[r][c],V_i[r][c]);
        printf("\n");
    }
#endif

    start_t = Clock_getTicks();
    float *ddmat[N];
    for (i =0; i<N; i++) ddmat[i] = (float*)malloc(L_FFT*sizeof(float));
    float DDest[N][N] = {0};

    for (k=0; k<L_FFT; k++)
    {
        ddmat[0][k] = sqrtsp(rx_save_a[0][k*N+0] * rx_save_a[0][k*N+0] + rx_save_ib[0][k*N+0] * rx_save_ib[0][k*N+0]);
        ddmat[1][k] = sqrtsp(rx_save_a[1][k*N+1] * rx_save_a[1][k*N+1] + rx_save_ib[1][k*N+1] * rx_save_ib[1][k*N+1]);
        ddmat[2][k] = sqrtsp(rx_save_a[2][k*N+2] * rx_save_a[2][k*N+2] + rx_save_ib[2][k*N+2] * rx_save_ib[2][k*N+2]);
        ddmat[3][k] = sqrtsp(rx_save_a[3][k*N+3] * rx_save_a[3][k*N+3] + rx_save_ib[3][k*N+3] * rx_save_ib[3][k*N+3]);
        if (ddmat[0][k] > DDest[0][0]) DDest[0][0] = ddmat[0][k];
        if (ddmat[1][k] > DDest[1][1]) DDest[1][1] = ddmat[1][k];
        if (ddmat[2][k] > DDest[2][2]) DDest[2][2] = ddmat[2][k];
        if (ddmat[3][k] > DDest[3][3]) DDest[3][3] = ddmat[3][k];
    }

#ifdef TEST_BSS
    printf("DDest =>\n");
    for (k=0; k<4; k++)
    {
    for (i=0; i<4; i++) printf("%1.4f  ", DDest[k][i]);
    printf("\n");
    }
#endif

    float Rx_BSS_r[N][N] = {0};
    float Rx_BSS_i[N][N] = {0};
    float tmp_r[N][N] = {0};
    float tmp_i[N][N] = {0};
    for (r=0; r<N; r++)
    {
        for (c=0; c<N; c++)
        {
            for (k=0; k<N; k++)
            {
                tmp_r[r][c] += DDest[r][k] * V_r[c][k];
                tmp_i[r][c] += -DDest[r][k] * V_i[c][k];
            }
        }
    }
    /*printf("DDest * V'=>\n");
    for (k=0; k<4; k++)
    {
    for (i=0; i<4; i++) printf("%1.4f  %1.4fi    ", tmp_r[k][i],tmp_i[k][i]);
    printf("\n");
    }*/

    for (r=0; r<N; r++)
    {
        for (c=0; c<N; c++)
        {
            for (k=0; k<N; k++)
            {
                Rx_BSS_r[r][c] += V_r[r][k] * tmp_r[k][c] - V_i[r][k] * tmp_i[k][c];
                Rx_BSS_i[r][c] += V_i[r][k] * tmp_r[k][c] + V_r[r][k] * tmp_i[k][c];
            }
        }
    }
#ifdef TEST_BSS
    printf("Rx_BSS=>\n");
    for (k=0; k<4; k++)
    {
    for (i=0; i<4; i++) printf("%1.4f  %1.4fi    ", Rx_BSS_r[k][i],Rx_BSS_i[k][i]);
    printf("\n");
    }

    Rx_BSS_r[0][0] = 1074974336.0000;
    Rx_BSS_r[1][0] = -358202624.0000;
    Rx_BSS_r[2][0] =   -1852634.8750;
    Rx_BSS_r[3][0] =  266395120.0000;

    Rx_BSS_r[0][1] = -358202592.0000;
    Rx_BSS_r[1][1] =  978419008.0000;
    Rx_BSS_r[2][1] =    1107929.7500;
    Rx_BSS_r[3][1] = -215978784.0000;

    Rx_BSS_r[0][2] = -1852634.5000;
    Rx_BSS_r[1][2] =  1107929.7500;
    Rx_BSS_r[2][2] =  1402049.1250;
    Rx_BSS_r[3][2] =  -379083.6875;

    Rx_BSS_r[0][3] =  266395136.0000;
    Rx_BSS_r[1][3] = -215978784.0000;
    Rx_BSS_r[2][3] =    -379083.7500;
    Rx_BSS_r[3][3] =  585923584.0000;

    Rx_BSS_i[0][0] = -15.9844;
    Rx_BSS_i[1][0] = -74223296.0000;
    Rx_BSS_i[2][0] = -2246112.2500;
    Rx_BSS_i[3][0] = -464563808.0000;

    Rx_BSS_i[0][1] = 74223264.0000;
    Rx_BSS_i[1][1] = 15.9844;
    Rx_BSS_i[2][1] = 1981468.6250;
    Rx_BSS_i[3][1] = -320128224.0000;

    Rx_BSS_i[0][2] = 2246112.2500;
    Rx_BSS_i[1][2] = -1981468.7500;
    Rx_BSS_i[2][2] = -0.0001;
    Rx_BSS_i[3][2] = 1395410.0000;

    Rx_BSS_i[0][3] = 464563840.0000;
    Rx_BSS_i[1][3] = 320128224.0000;
    Rx_BSS_i[2][3] = -1395410.0000;
    Rx_BSS_i[3][3] = 0.0000;
#endif



    float A[N*N*2];
    float U[N*N*2];
    float V[N*N*2];
    float U1[N*N*2];
    float Lam_vec[N*2];
    float superdiag[N*2];
    for (r=0; r<N; r++)
    {
        for (c=0; c<N; c++)
        {
            A[(2*N*r)+c*2] = Rx_BSS_r[r][c];
            A[(2*N*r)+c*2+1] = Rx_BSS_i[r][c];
        }
    }

    int rv = DSPF_sp_svd_cmplx(N, N, A, U, V, U1, Lam_vec, superdiag);
    /*printf("DSPF_svd_cmplx return value: %d\n", rv);
    printf("Lam_vec: %f %fi  %f %fi  %f %fi  %f %fi\n",Lam_vec[0],Lam_vec[1],
                                                       Lam_vec[2],Lam_vec[3],
                                                       Lam_vec[4],Lam_vec[5],
                                                       Lam_vec[6],Lam_vec[7]);*/

    // MDL -
    float MDL[N] = {0};
    int Ns;
    for (Ns = 0; Ns < N; Ns++)
    {
        float tmp_sum = 0;
        float tmp_mean = 0;
        for (i=Ns; i <N; i++)
        {
            tmp_sum += logsp(Lam_vec[i*2]);
            tmp_mean += Lam_vec[i*2];
        }
        float LL2 = (float)(-N_FRAME) * tmp_sum + (float)(N_FRAME) * (float)(N-Ns) * logsp(tmp_mean / (float)(N-Ns));
        MDL[Ns] = LL2 + (Ns*(2*N-Ns)*logsp(N_FRAME)) / 2.0;
    }
    //printf("MDL=>\n%f\n%f\n%f\n%f\n",MDL[0],MDL[1],MDL[2],MDL[3]);
    int NsEstMDL = 0;
    if ((MDL[0] < MDL[1]) && (MDL[0] < MDL[2]) && (MDL[0] < MDL[3]))
    {
        NsEstMDL = 0;
    }
    else if ((MDL[1] < MDL[0]) && (MDL[1] < MDL[2]) && (MDL[1] < MDL[3]))
    {
        NsEstMDL = 1;
    }
    else if ((MDL[2] < MDL[0]) && (MDL[2] < MDL[1]) && (MDL[2] < MDL[3]))
    {
        NsEstMDL = 2;
    }
    else if ((MDL[3] < MDL[0]) && (MDL[3] < MDL[1]) && (MDL[3] < MDL[2]))
    {
        NsEstMDL = 3;
    }
    // end MDL

    float null_depth_increase_dB = 0; // This value controls the nulls depth beyound what the algorithm set. Null depth change in dB
    float null_depth_increase = powsp(10, null_depth_increase_dB/10.0);

    for (i=0; i<NsEstMDL; i++)
    {
        Lam_vec[2*i] *= null_depth_increase;
    }
//    printf("Lam_vec: %f   %f   %f   %f \n",Lam_vec[0],Lam_vec[2],Lam_vec[4],Lam_vec[6]);

    float alpha = -1.0/2.0;
    float final_Rx_BSS_r[N][N] = {0};
    float final_Rx_BSS_i[N][N] = {0};
    float Rx_BSS_inv_sqrt_r[N][N] = {0};
    float Rx_BSS_inv_sqrt_i[N][N] = {0};

    float tmp2_r[N][N] = {0};
    float tmp2_i[N][N] = {0};
    float tmp3_r[N][N] = {0};
    float tmp3_i[N][N] = {0};


    for (r = 0; r<N; r++)
    {
        for (c=0; c<N; c++)
        {
            tmp2_r[r][c] = U[(2*N*r)+2*c] * Lam_vec[2*c];
            tmp2_i[r][c] = U[(2*N*r)+2*c+1] * Lam_vec[2*c];
            tmp3_r[r][c] = U[(2*N*r)+2*c] * powsp(Lam_vec[2*c],alpha);
            tmp3_i[r][c] = U[(2*N*r)+2*c+1] * powsp(Lam_vec[2*c],alpha);
        }
    }
    /*printf("U*diag(Lam_vec)=>\n");
    for (k=0; k<4; k++)
    {
    for (i=0; i<4; i++) printf("%1.4f  %1.4fi    ", tmp2_r[k][i],tmp2_i[k][i]);
    printf("\n");
    }*/
    for (r = 0; r<N; r++)
    {
        for (c=0; c<N; c++)
        {
            for (k=0; k<N; k++)
            {
                final_Rx_BSS_r[r][c] += tmp2_r[r][k] * U[(2*N*c)+2*k] + tmp2_i[r][k] * U[(2*N*c)+2*k+1];
                final_Rx_BSS_i[r][c] += -tmp2_r[r][k] * U[(2*N*c)+2*k+1] + tmp2_i[r][k] * U[(2*N*c)+2*k];
                Rx_BSS_inv_sqrt_r[r][c] += tmp3_r[r][k] * U[(2*N*c)+2*k] + tmp3_i[r][k] * U[(2*N*c)+2*k+1];
                Rx_BSS_inv_sqrt_i[r][c] += -tmp3_r[r][k] * U[(2*N*c)+2*k+1] + tmp3_i[r][k] * U[(2*N*c)+2*k];
            }
        }
    }

#ifdef TEST_BSS
    printf("Rx_BSS = U*Lam*U'=>\n");
        for (k=0; k<4; k++)
        {
        for (i=0; i<4; i++) printf("%1.4f  %1.4fi    ", final_Rx_BSS_r[k][i],final_Rx_BSS_i[k][i]);
        printf("\n");
        }

        printf("Rx_BSS_inv =>\n");
            for (k=0; k<4; k++)
            {
            for (i=0; i<4; i++) printf("%1.10f  %1.10fi    ", Rx_BSS_inv_sqrt_r[k][i],Rx_BSS_inv_sqrt_i[k][i]);
            printf("\n");
            }



    // TODO: NsEstMDL == 0 Set Rx_BSS to Rx_BSS = mean(diag(Rx_BSS)) * eye(size(Rx_BSS,1));  % Add deepening the null.
    printf("Norm=>\n");
#endif

    float w_BSS_r[N][N] = {0};
    float w_BSS_i[N][N] = {0};
    float norm_w_BSS[N] = {0};
    for (k=0; k<N; k++)
    {
        for (i=0;i<N;i++)
        {
            norm_w_BSS[k] += (Rx_BSS_inv_sqrt_r[i][k] * Rx_BSS_inv_sqrt_r[i][k] + Rx_BSS_inv_sqrt_i[i][k] * Rx_BSS_inv_sqrt_i[i][k]);
            //printf("k,i = (%d,%d)=%f  ",k,i, norm_w_BSS[k]);
            //printf("(%1.4f,%1.4fi) abs=%f abs^2=%f \n",Rx_BSS_inv_sqrt_r[i][k], Rx_BSS_inv_sqrt_i[i][k], sqrtsp((Rx_BSS_inv_sqrt_r[i][k] * Rx_BSS_inv_sqrt_r[i][k] + Rx_BSS_inv_sqrt_i[i][k] * Rx_BSS_inv_sqrt_i[i][k])), (Rx_BSS_inv_sqrt_r[i][k] * Rx_BSS_inv_sqrt_r[i][k] + Rx_BSS_inv_sqrt_i[i][k] * Rx_BSS_inv_sqrt_i[i][k]) );
        }
        norm_w_BSS[k] = sqrtsp(norm_w_BSS[k]);
//        printf("%f\n", norm_w_BSS[k]);
    }
    for (r=0; r<N; r++)
    {
        for (c=0; c<N; c++)
        {
            w_BSS_r[r][c] = Rx_BSS_inv_sqrt_r[r][c] / norm_w_BSS[c];
            w_BSS_i[r][c] = Rx_BSS_inv_sqrt_i[r][c] / norm_w_BSS[c];
        }
    }
    end_t = Clock_getTicks();
//   printf("\n Time MDL+BSS %d ticks\n", end_t - start_t);

#ifdef TEST_BSS
    printf("Rx_BSS=>\n");
        for (k=0; k<4; k++)
        {
        for (i=0; i<4; i++) printf("%1.4f  %1.4fi    ", w_BSS_r[k][i],w_BSS_i[k][i]);
        printf("\n");
        }
/*
        printf("Rx_BSS_inv_sqrt=>\n");
            for (k=0; k<4; k++)
            {
            for (i=0; i<4; i++) printf("%1.4f  %1.4fi    ", Rx_BSS_inv_sqrt_r[k][i],Rx_BSS_inv_sqrt_i[k][i]);
            printf("\n");
            }*/
#endif

    // Calculate the angle of ch.4 (ref ch.2)
    /*float angle4 = atan2sp(-w_BSS_i[3][1], w_BSS_r[3][1]) * 180 /MY_PI;
    float angle2;
    float gain4 = w_BSS_r[3][1]*w_BSS_r[3][1] + w_BSS_i[3][1]*w_BSS_i[3][1] ;
    if (-w_BSS_i[3][1] < 0)
    {
        angle2 = 270;
    }
    else
    {
        angle2 = 90;
    }*


    float adjust_angle4 = -6;
    float adjust_gain4 = -0.5;*/
    float w_BSS_sc_r[N][N] = {0};
    float w_BSS_sc_i[N][N] = {0};


    //printf("w_BSS_sc=>\n");
                for (k=0; k<4; k++)
                {
                for (i=0; i<4; i++) {
                    float rrr, iii;
                    div_cmplx(w_BSS_r[k][i], w_BSS_i[k][i], w_BSS_r[1][0], w_BSS_i[1][0], &rrr, &iii);
                    w_BSS_sc_r[k][i] = rrr;
                    w_BSS_sc_i[k][i] = iii;
      //              printf("%1.4f  %1.4fi    ", rrr, iii);

                }
        //        printf("\n");
                }

    float w_BSS__r[N];
    float w_BSS__i[N];
    for (k=0; k<N; k++)
    {
        float rrr, iii;
        div_cmplx(w_BSS_sc_r[k][0], w_BSS_sc_i[k][0], w_BSS_sc_r[1][0], w_BSS_sc_i[1][0], &rrr, &iii);
        w_BSS__r[k] = rrr;
        w_BSS__i[k] = iii;
    }

//    printf("Result =>\n");
    for (k=0; k<N; k++)
    {
        selectedRfic[k].Angle = (uint16_t)(10.0 * atan2sp(-w_BSS__i[k], w_BSS__r[k]) * 180 /MY_PI);
        selectedRfic[k].Amplitude = (uint16_t)(100.0 * sqrtsp(w_BSS__i[k] * w_BSS__i[k] + w_BSS__r[k] * w_BSS__r[k]));
//        if (k!=2) printf("%1.4f   %2.4f\n", abs_arr[k], angle_arr[k]);
    }


    fflush(stdout);

    for (i =0; i<N; i++) free(ddmat[i]);
// TODO: Convert angle_arr[k] and abs_arr[k] to ints by multiplying by 100



 ////////////////////////////
    BlickLed ^= true;
    if (BlickLed)
        *GPIO_SET_REG = GPIO_PIN[GPIO_DSP_READY_PIN];
    else
        *GPIO_CLR_REG = GPIO_PIN[GPIO_DSP_READY_PIN];
    return 0;
}
//--------------------------------------------------------

#ifdef CALCK
void joint_diag(float jthresh)
{
    int r,c,i,j,k;
    float V_r[N][N];
    float V_i[N][N];

    /*printf("\n\n\n");
    for (r=0; r<N; r++)
    {for (i=0; i<N*L_FFT; i++) printf("%f, ", a[r][i]); printf("\n");}
    printf("\n\n\n");*/

    float b_r[3][3] = {{1,0,0},{0,1,1},{0,0,0}};
    float b_i[3][3] = {{0,0,0},{0,0,0},{0,-1,1}};

    for (r=0; r<N; r++)
    {
        for (c=0; c<N; c++)
        {
            V_r[r][c] = 0;
            V_i[r][c] = 0;
        }
    }
    for (r=0; r<N; r++) V_r[r][r] = 1;

    float G_r[2][2] = {0};
    float G_i[2][2] = {0};

    float Gt_r[2][2] = {0};
    float Gt_i[2][2] = {0};

    float *g_r[3];
    float *g_i[3];
    for (i = 0; i < N; i++)
    {
        g_r[i] = (float*)malloc(L_FFT*sizeof(float));
        g_i[i] = (float*)malloc(L_FFT*sizeof(float));
    }

    float tmp_r[3][3];
    float tmp_i[3][3];
    float tmp2_r[3][3];
    float tmp2_i[3][3];
    float tmp3_r[3][3];
    float tmp3_i[3][3];
//    float tmp4_r[N][2];
//    float tmp4_i[N][2];
//    float *tmp5_r[2];
//    float *tmp5_i[2];
//    tmp5_r[0] = (float*)malloc(N*L_FFT*sizeof(float));
//    tmp5_i[0] = (float*)malloc(N*L_FFT*sizeof(float));
//    tmp5_r[1] = (float*)malloc(N*L_FFT*sizeof(float));
//    tmp5_i[1] = (float*)malloc(N*L_FFT*sizeof(float));
//    float *tmp6_r[N];
//    float *tmp6_i[N];
//    float *tmp7_r[N];
//    float *tmp7_i[N];
//    for (i = 0; i < N; i++)
//    {
//        tmp6_r[i] = (float*)malloc(L_FFT*sizeof(float));
//        tmp6_i[i] = (float*)malloc(L_FFT*sizeof(float));
//        tmp7_r[i] = (float*)malloc(L_FFT*sizeof(float));
//        tmp7_i[i] = (float*)malloc(L_FFT*sizeof(float));
//    }



    int p;
    int q;
    int Ip;
    int Iq;
    bool encore = true;
    int zz=0;
    while(zz<15)
    {
        zz++;
        encore=false;
        //p=1;q=p+1;
        for (p = 0; p<N-1; p++)
        {
            for (q = p+1; q<N; q++)
            {
                Ip=p; Iq=q;

                for (i=0; i<L_FFT; i++)
                {
                    g_r[0][i] = rx_save_a[p][Ip] - rx_save_a[q][Iq];
                    g_i[0][i] = rx_save_ib[p][Ip] - rx_save_ib[q][Iq];
                    g_r[1][i] = rx_save_a[p][Iq];
                    g_i[1][i] = rx_save_ib[p][Iq];
                    g_r[2][i] = rx_save_a[q][Ip];
                    g_i[2][i] = rx_save_ib[q][Ip];
                    Ip += N;
                    Iq += N;
                }
                /*printf("g_ =>\n");
                for (r = 0; r < 3; r++)
                {
                    for (c = 0; c < 5; c++)
                    {
                        printf("%1.4f  %1.4fi   ", g_r[r][c], g_i[r][c]);

                    }
                    printf("\n");
                }
                printf("\n\n");*/



                for (i=0; i<3; i++)
                    for (j=0; j<3; j++)
                    {
                        tmp_r[i][j] = 0;
                        tmp_i[i][j] = 0;
                        for (k=0; k<L_FFT; k++)
                        {
                            tmp_r[i][j] += g_r[i][k] * g_r[j][k] + g_i[i][k] * g_i[j][k];
                            tmp_i[i][j] += -g_r[i][k] * g_i[j][k] + g_i[i][k] * g_r[j][k];
                        }
                    }
                /*printf("tmp_r =>\n");
                for (r = 0; r < 3; r++)
                {
                    for (c = 0; c < 3; c++)
                    {
                        printf("%1.4f  %1.4fi   ", tmp_r[r][c], tmp_i[r][c]);

                    }
                    printf("\n");
                }
                printf("\n\n");*/

                for (i=0; i<3; i++)
                    for (j=0; j<3; j++)
                    {
                        tmp2_r[i][j] = 0;
                        tmp2_i[i][j] = 0;
                        for (k=0; k<3; k++)
                        {
                            tmp2_r[i][j] += b_r[i][k] * tmp_r[j][k] + b_i[i][k] * tmp_i[j][k];
                            tmp2_i[i][j] += -b_r[i][k] * tmp_i[j][k] + b_i[i][k] * tmp_r[j][k];
                        }
                    }
                /*printf("tmp2_r =>\n");
                for (r = 0; r < 3; r++)
                {
                    for (c = 0; c < 3; c++)
                    {
                        printf("%1.4f  %1.4fi   ", tmp2_r[r][c], tmp2_i[r][c]);

                    }
                    printf("\n");
                }
                printf("\n\n");*/


                for (i=0; i<3; i++)
                    for (j=0; j<3; j++)
                    {
                        tmp3_r[i][j] = 0;
                        tmp3_i[i][j] = 0;
                        for (k=0; k<3; k++)
                        {
                            tmp3_r[i][j] += tmp2_r[i][k] * b_r[j][k] + tmp2_i[i][k] * b_i[j][k];
                            tmp3_i[i][j] += -tmp2_r[i][k] * b_i[j][k] + tmp2_i[i][k] * b_r[j][k];
                        }
                    }
                /*printf("tmp3_r =>\n");
                for (r = 0; r < 3; r++)
                {
                    for (c = 0; c < 3; c++)
                    {
                        printf("%1.4f  %1.4fi   ", tmp3_r[r][c], tmp3_i[r][c]);

                    }
                    printf("\n");
                }
                printf("\n\n");*/


                //float valre[3];
                int max_idx;
                eigen3x3(tmp3_r, &max_idx);
                //printf("eig: %1.4f  %1.4f  %1.4f  nmax=%d\n", valre[0], valre[1], valre[2],max_idx);
                float normalize;
                normalize = sqrtsp(eivec[0][max_idx] * eivec[0][max_idx] + eivec[1][max_idx] * eivec[1][max_idx] + eivec[2][max_idx] * eivec[2][max_idx]);

                float angles[3];
                angles[0] = eivec[0][max_idx] / normalize;
                angles[1] = eivec[1][max_idx] / normalize;
                angles[2] = eivec[2][max_idx] / normalize;
                if (angles[0] < 0)
                {
                    angles[0] = -angles[0];
                    angles[1] = -angles[1];
                    angles[2] = -angles[2];
                }
                //printf("(p,q)=(%d,%d) angles=%f,%f,%f\n",p,q,angles[0],angles[1],angles[2]);
                float c = sqrtsp(0.5+angles[0]/2);
                float s_r = 0.5*angles[1]/c;
                float s_i = -0.5*angles[2]/c;
                float abs_s =  sqrtsp(s_r*s_r+s_i*s_i);
                //printf("c=%f, s_r=%f, s_i=%f, abs(s)=%f\n",c,s_r,s_i,abs_s);

                if (abs_s > jthresh)
                {
                    /*printf("pair(%d,%d)\n",p+1,q+1);
                    printf("angles:%1.4f %1.4f %1.4f\n",angles[0],angles[1],angles[2]);*/
                    //printf("(p,q)=(%d,%d) abs(s)=%1.12f\n",p,q,abs_s);
                    encore=true;
                    G_r[0][0] = c; G_r[0][1] = -s_r;
                    G_r[1][0] = s_r; G_r[1][1] = c;
                    G_i[0][0] = 0; G_i[0][1] = s_i;
                    G_i[1][0] = s_i; G_i[1][1] = 0;

                    Gt_r[0][0] = c; Gt_r[0][1] = s_r;
                    Gt_r[1][0] = -s_r; Gt_r[1][1] = c;
                    Gt_i[0][0] = 0; Gt_i[0][1] = -s_i;
                    Gt_i[1][0] = -s_i; Gt_i[1][1] = 0;

/*---------------------------------------------------------------------------------------------
                    for (k=0; k<N; k++)
                    {
                        tmp4_r[k][0] = 0;
                        tmp4_i[k][0] = 0;
                        tmp4_r[k][1] = 0;
                        tmp4_i[k][1] = 0;
                    }

                    for (k=0; k<N*L_FFT; k++)
                    {
                        tmp5_r[0][k] = 0;
                        tmp5_i[0][k] = 0;

                        tmp5_r[1][k] = 0;
                        tmp5_i[1][k] = 0;
                    }



                    for (k=0; k<N; k++)
                    {
                        tmp4_r[k][0] += V_r[k][p] * G_r[0][0] - V_i[k][p] * G_i[0][0] + V_r[k][q] * G_r[1][0] - V_i[k][q] * G_i[1][0];
                        tmp4_i[k][0] += V_r[k][p] * G_i[0][0] + V_i[k][p] * G_r[0][0] + V_r[k][q] * G_i[1][0] + V_i[k][q] * G_r[1][0];

                        tmp4_r[k][1] += V_r[k][p] * G_r[0][1] - V_i[k][p] * G_i[0][1] + V_r[k][q] * G_r[1][1] - V_i[k][q] * G_i[1][1];
                        tmp4_i[k][1] += V_r[k][p] * G_i[0][1] + V_i[k][p] * G_r[0][1] + V_r[k][q] * G_i[1][1] + V_i[k][q] * G_r[1][1];

                    }
                    // Update V(:,pair) pair = [p,q]  V(:,pair) = V(:,pair)*G;
                    for (k=0; k<N; k++)
                    {
                        V_r[k][p] = tmp4_r[k][0];
                        V_i[k][p] = tmp4_i[k][0];
                        V_r[k][q] = tmp4_r[k][1];
                        V_i[k][q] = tmp4_i[k][1];
                        //printf("%1.4f + %1.4fi    %1.4f + %1.4fi\n",V_r[k][p],V_i[k][p],V_r[k][q],V_i[k][q]);
                    }

                    for (k=0; k<N*L_FFT; k++)
                    {
                        tmp5_r[0][k] += Gt_r[0][0] * rx_save_a[p][k] - Gt_i[0][0] * rx_save_ib[p][k] + Gt_r[0][1] * rx_save_a[q][k] - Gt_i[0][1] * rx_save_ib[q][k];
                        tmp5_i[0][k] += Gt_r[0][0] * rx_save_ib[p][k] + Gt_i[0][0] * rx_save_a[p][k] + Gt_r[0][1] * rx_save_ib[q][k] + Gt_i[0][1] * rx_save_a[q][k];

                        tmp5_r[1][k] += Gt_r[1][0] * rx_save_a[p][k] - Gt_i[1][0] * rx_save_ib[p][k] + Gt_r[1][1] * rx_save_a[q][k] - Gt_i[1][1] * rx_save_ib[q][k];
                        tmp5_i[1][k] += Gt_r[1][0] * rx_save_ib[p][k] + Gt_i[1][0] * rx_save_a[p][k] + Gt_r[1][1] * rx_save_ib[q][k] + Gt_i[1][1] * rx_save_a[q][k];
                    }
                    // Update A(pair, :) = G' * A(pair, :)
                    for (k=0; k<N*L_FFT; k++)
                    {
                        rx_save_a[p][k] = tmp5_r[0][k];
                        rx_save_a[q][k] = tmp5_r[1][k];
                        rx_save_ib[p][k] = tmp5_i[0][k];
                        rx_save_ib[q][k] = tmp5_i[1][k];
                    }

                    // A(:,[Ip Iq])    = [ c*A(:,Ip)+s*A(:,Iq) -conj(s)*A(:,Ip)+c*A(:,Iq) ] ;
                    Ip=p;
                    Iq=q;
                    for (i=0; i<L_FFT; i++)
                    {
                        for (r=0; r<N; r++)
                        {
                            tmp6_r[r][i] = c*rx_save_a[r][Ip] + s_r*rx_save_a[r][Iq] - s_i*rx_save_ib[r][Iq];
                            tmp6_i[r][i] = c*rx_save_ib[r][Ip] + s_i*rx_save_a[r][Iq] + s_r*rx_save_ib[r][Iq];
                            tmp7_r[r][i] = -s_r*rx_save_a[r][Ip] -s_i*rx_save_ib[r][Ip] + c*rx_save_a[r][Iq];
                            tmp7_i[r][i] = -s_r*rx_save_ib[r][Ip] +s_i*rx_save_a[r][Ip] +c*rx_save_ib[r][Iq];
                        }
                        Ip += N;
                        Iq += N;
                    }

                    Ip=p;
                    Iq=q;
                    for (i=0; i<L_FFT; i++)
                    {
                        for (r=0; r<N; r++)
                        {
                            rx_save_a[r][Ip] = tmp6_r[r][i];
                            rx_save_ib[r][Ip] = tmp6_i[r][i];
                            rx_save_a[r][Iq] = tmp7_r[r][i];
                            rx_save_ib[r][Iq] = tmp7_i[r][i];
                        }
                        Ip += N;
                        Iq += N;
                    }
---------------------------------------------------------------------------------------------------*/

                    for (k=0; k<N; k++)
                    {
                        V_r[k][p] += V_r[k][p] * G_r[0][0] - V_i[k][p] * G_i[0][0] + V_r[k][q] * G_r[1][0] - V_i[k][q] * G_i[1][0];
                        V_i[k][p] += V_r[k][p] * G_i[0][0] + V_i[k][p] * G_r[0][0] + V_r[k][q] * G_i[1][0] + V_i[k][q] * G_r[1][0];
                        V_r[k][q] += V_r[k][p] * G_r[0][1] - V_i[k][p] * G_i[0][1] + V_r[k][q] * G_r[1][1] - V_i[k][q] * G_i[1][1];
                        V_i[k][q] += V_r[k][p] * G_i[0][1] + V_i[k][p] * G_r[0][1] + V_r[k][q] * G_i[1][1] + V_i[k][q] * G_r[1][1];
                        //printf("%1.4f + %1.4fi    %1.4f + %1.4fi\n",V_r[k][p],V_i[k][p],V_r[k][q],V_i[k][q]);
                    }

                    for (k=0; k<N*L_FFT; k++)
                    {
                        rx_save_a[p][k] += Gt_r[0][0] * rx_save_a[p][k] - Gt_i[0][0] * rx_save_ib[p][k] + Gt_r[0][1] * rx_save_a[q][k] - Gt_i[0][1] * rx_save_ib[q][k];
                        rx_save_a[q][k] += Gt_r[1][0] * rx_save_a[p][k] - Gt_i[1][0] * rx_save_ib[p][k] + Gt_r[1][1] * rx_save_a[q][k] - Gt_i[1][1] * rx_save_ib[q][k];
                        rx_save_ib[p][k] += Gt_r[0][0] * rx_save_ib[p][k] + Gt_i[0][0] * rx_save_a[p][k] + Gt_r[0][1] * rx_save_ib[q][k] + Gt_i[0][1] * rx_save_a[q][k];
                        rx_save_ib[q][k] += Gt_r[1][0] * rx_save_ib[p][k] + Gt_i[1][0] * rx_save_a[p][k] + Gt_r[1][1] * rx_save_ib[q][k] + Gt_i[1][1] * rx_save_a[q][k];
                    }

                    Ip=p;
                    Iq=q;
                    for (i=0; i<L_FFT; i++)
                    {
                        for (r=0; r<N; r++)
                        {
                            rx_save_a[r][Ip] = c*rx_save_a[r][Ip] + s_r*rx_save_a[r][Iq] - s_i*rx_save_ib[r][Iq];
                            rx_save_ib[r][Ip] = c*rx_save_ib[r][Ip] + s_i*rx_save_a[r][Iq] + s_r*rx_save_ib[r][Iq];
                            rx_save_a[r][Iq] = -s_r*rx_save_a[r][Ip] -s_i*rx_save_ib[r][Ip] + c*rx_save_a[r][Iq];
                            rx_save_ib[r][Iq] = -s_r*rx_save_ib[r][Ip] +s_i*rx_save_a[r][Ip] +c*rx_save_ib[r][Iq];
                        }
                        Ip += N;
                        Iq += N;
                    }
                } //if (abs_s > jthresh)
            } //q
        } //p

        //printf("------%d\n",zz);
        //break;
    } //while

    /*for (r = 0; r < 4; r++)
    {
        for (c = 0; c < 4; c++)
        {
            printf("%1.4f + j%1.4f   ", V_r[r][c], V_i[r][c]);

        }
        printf("\n");
    }
    printf("\n");*/

/*  for (int k = 0; k < N*L_FFT; k++)
    {
        printf("%1.4f + j%1.4f\n", a[3][k],ib[3][k]);
    }*/
    //printf("Z:%d\n",zz);

    for (i = 0; i < N; i++)
    {
        free(g_r[i]);
        free(g_i[i]);
    }
//    free(tmp5_r[0]);
//    free(tmp5_i[0]);
//    free(tmp5_r[1]);
//    free(tmp5_i[1]);
//    for (i = 0; i < N; i++)
//    {
//        free(tmp6_r[i]);
//        free(tmp6_i[i]);
//        free(tmp7_r[i]);
//        free(tmp7_i[i]);
//    }
}


void eigen3x3(float C[3][3], int *max_idx)
{
    float x1, x2;
    float phi;
    float m1, m2, m3;

    float eigvals[3];

//#ifdef DO_RUN_GCH

    x1 = C[0][0] * C[0][0]  + C[1][1] * C[1][1] + C[2][2] * C[2][2] - C[0][0] * C[1][1] - C[0][0] * C[2][2] - C[1][1]*C[2][2] +3 * (C[0][1] * C[0][1] + C[0][2] * C[0][2] + C[1][2]*C[1][2]);
    x2 = -(2*C[0][0] - C[1][1] - C[2][2])*(2*C[1][1] - C[0][0] - C[2][2])*(2*C[2][2] - C[0][0] - C[1][1]) + 9 * ((2*C[2][2] - C[0][0] - C[1][1])*C[0][1]*C[0][1] +(2*C[1][1] - C[0][0] - C[2][2])*C[0][2]*C[0][2] +(2*C[0][0] - C[1][1] - C[2][2])*C[1][2]*C[1][2]) - 54 * C[0][1] * C[1][2] * C[0][2];

    //printf("x1: %f \nx2: %f \n", x1, x2);

    if (x2 > 0)
    {
        phi = (float)(atansp(sqrtsp(4*x1*x1*x1-x2*x2)/x2));
    }
    else if (x2 < 0)
    {
        phi = (float)(atansp(sqrtsp(4*x1*x1*x1-x2*x2)/x2) + MY_PI);
    }
    else
    {
        phi = MY_PI / 2.0;
    }
    //printf("phi : %f\n", phi);

    eigvals[0] = (C[0][0] + C[1][1] + C[2][2] - 2*sqrtsp(x1)*cossp(phi/3.0))/3.0;
    eigvals[1] = (C[0][0] + C[1][1] + C[2][2] + 2*sqrtsp(x1)*cossp((phi-MY_PI)/3.0))/3.0;
    eigvals[2] = (C[0][0] + C[1][1] + C[2][2] + 2*sqrtsp(x1)*cossp((phi+MY_PI)/3.0))/3.0;

    m1 = (C[0][1]*(C[2][2] - eigvals[0])-C[1][2]*C[0][2])/(C[0][2]*(C[1][1]-eigvals[0])-C[0][1]*C[1][2]);
    m2 = (C[0][1]*(C[2][2] - eigvals[1])-C[1][2]*C[0][2])/(C[0][2]*(C[1][1]-eigvals[1])-C[0][1]*C[1][2]);
    m3 = (C[0][1]*(C[2][2] - eigvals[2])-C[1][2]*C[0][2])/(C[0][2]*(C[1][1]-eigvals[2])-C[0][1]*C[1][2]);

    eivec[0][0] = (eigvals[0] - C[2][2] - C[1][2]*m1) / C[0][2];
    eivec[1][0] = m1;
    eivec[2][0] = 1;

    eivec[0][1] = (eigvals[1] - C[2][2] - C[1][2]*m2) / C[0][2];
    eivec[1][1] = m2;
    eivec[2][1] = 1;

    eivec[0][2] = (eigvals[2] - C[2][2] - C[1][2]*m3) / C[0][2];
    eivec[1][2] = m3;
    eivec[2][2] = 1;

    if (eigvals[0] > eigvals[1] && eigvals[0] > eigvals[2]) {*max_idx=0;}
    else {
        if (eigvals[1] > eigvals[0] && eigvals[1] > eigvals[2]) {*max_idx=1;}
        else { *max_idx=2;}
    }
//#endif

}

#endif //CALCK
//-----------------------------------------------------------------------

void div_cmplx(float a, float b, float c, float d, float *r_r, float *r_i)
{
    *r_r = (a*c + b*d) / (c*c + d*d);
    *r_i = (-a*d + b*c) / (c*c + d*d);
}
//----------------------------------------------------------------------
